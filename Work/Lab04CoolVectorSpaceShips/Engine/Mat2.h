#pragma once
#include "ExportHeader.h"
#include <iostream>
#include "Vec2.h"
#include "Engine.h"





namespace Engine
{
	class ENGINE_SHARED Mat2
	{
	public:
		Mat2() : Mat2(Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f)) {};
		Mat2(Vec2 leftCol, Vec2 rightCol) 
		{
			matrixArray[0] = leftCol.x;
			matrixArray[1] = rightCol.x;
			matrixArray[2] = leftCol.y;     
			matrixArray[3] = rightCol.y;
		};
		~Mat2();

		static Mat2 Rotate(float degrees);
		static Mat2 Scale(float scale);//uniform
		static Mat2 Scale(Vec2& scale);//nonuniform
		static Mat2 Scale(float scaleX, float scaleY); //nonuniform
		static Mat2 ScaleX(flaot scale);
		static Mat2 ScaleY(flaot scale);

		Mat2 operator*(Mat2& mat2) const;
		Vec2 operator*(Vec2& vec2) const;
		Mat2 operator*(flaot scale) const;
		bool operator==(const Mat2& mat2) const;




		float matrixArray[4];

	private:
		static bool CompareFloats(float a, float b);

	};
	inline Mat2 operator*(float scaler, const Mat2& v)
	{
		return v * scaler;
	}
}



