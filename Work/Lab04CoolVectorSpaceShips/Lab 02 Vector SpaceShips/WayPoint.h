#pragma once
#include "Shape.h"
#include "Factory.h"
#include "GameObject.h"

class WayPoint :
	public GameObject
{
public:
	WayPoint(int num, bool isRow, int row);
	~WayPoint();

	bool WaypointDraw(Core::Graphics g);


	Shape shape;
	int order;
	int row;
	bool isRow;
private:
	void Init(int num);

};

