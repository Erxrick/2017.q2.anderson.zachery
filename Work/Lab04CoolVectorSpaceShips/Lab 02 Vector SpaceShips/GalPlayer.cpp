#include "GalPlayer.h"
#include <string>


GalPlayer::GalPlayer()
{
	ShipInit();
}


GalPlayer::~GalPlayer()
{
}

void GalPlayer::ShipInit()
{
	shape.ShapeInit(Factory::SpaceShipShape(), Factory::SpaceShipSize(), 1);
	m_pos = m_pos + m_startingPos;

}

bool GalPlayer::ShipUpdate(float dt, Engine::Vec2 velocity)
{
	m_pos = m_pos + (velocity * dt);
	return true;
}

bool GalPlayer::ShipUpdate(float dt)
{
	if (lives > 0)
	{
		Vec2 returnVec = Vec2(0.0f, 0.0f);
		if (Core::Input::IsPressed(Core::Input::KEY_LEFT))
		{
			returnVec = returnVec + Vec2(-vel, 0.0f);
		}
		if (Core::Input::IsPressed(Core::Input::KEY_RIGHT))
		{
			returnVec = returnVec + Vec2(vel, 0.0f);
		}
		m_pos = m_pos + (returnVec * dt);
		transform = Mat3::Translation(m_pos) * (Mat3::Rotation(3.14159f) * Mat3::Scale((float)m_scale));
		Shoot(dt);
		if (AmIGonnaDie(dt) == true) lives--;
	}
	return false;
}
void GalPlayer::Shoot(float dt)
{
	static float acumTime = 0;
	acumTime += dt;
	if (Core::Input::IsPressed(Core::Input::KEY_UP) && acumTime > 0.7f)
	{
		BM->ShootBullet(m_pos, Vec2(0, -100), false);
		acumTime = 0.0f;
	}
}
bool GalPlayer::AmIGonnaDie(float dt)
{

	return BM->WithinRange(m_pos, false);
}
Engine::Vec2 GalPlayer::ShipPosition()
{
	return m_pos;
}
bool GalPlayer::ShipDraw(Core::Graphics & g)
{
	if(lives > 0)
	shape.DrawShape(g, transform);
	g.SetColor(RGB(255, 255, 255));
	g.DrawString(570, 20, std::to_string(lives).c_str());
	g.DrawString(520, 20, "Lives:");
	if (lives == 0) {
	g.SetColor(RGB(255, 255, 255));
	g.DrawString(375, 400, "GAME OVER");
	}

	return false;
}
