#include "Shape.h"


Shape::Shape()
{

}

Shape::~Shape()
{
}




void Shape::DrawShape(Core::Graphics & g, Engine::Mat3 transform)
{
	g.SetColor(RGB(color1, color2, color3));
	for (int i = 0; i < m_numShapePoints; i++)
	{
		shape[i] = transform * m_shape[i];
	}
	for (int i = 0; i < m_numShapePoints-2; i += 2)
	{
		g.DrawLine(shape[i].x, shape[i].y, shape[i+1].x, shape[i+1].y);
	}
}
void Shape::DrawShape(Core::Graphics & g, const Engine::Vec2& position, int scale)
{
	g.SetColor(RGB(color1, color2, color3));

	for (int i = 0; i < m_numShapePoints - 2; i += 2)
	{
		g.DrawLine((m_shape[i].x * scale) + position.x, (m_shape[i].y * scale) + position.y, (m_shape[i + 1].x * scale) + position.x, (m_shape[i + 1].y * scale) + position.y);

	}
}

bool Shape::ShapeInit(Engine::Vec2* shape, int numPoints, int scaler)
{
	m_numShapePoints = numPoints;
	m_shape = shape;
	for (int j = 0; j < m_numShapePoints; ++j)
	{
		m_shape[j] = (m_shape[j] * (float)scaler);
	}
	return true;
}
