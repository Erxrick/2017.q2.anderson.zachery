#pragma once
#include "GameObject.h"
#include "Vec2.h"
#include "Mat3.h"
using namespace Engine;
class Turret :
	public GameObject
{
public:
	Turret();
	~Turret();

	bool DrawShape(Core::Graphics& g, Engine::Vec2 pos, Engine::Vec2 center);

	void SetColor(int c1, int c2, int c3);


	bool playerOrEnemy;
	Engine::Vec2 origin;
	Engine::Vec2 endPoint;
	Engine::Vec2* lockedOnto;
	int m_scale{ 5 };
	Mat3 m_transform;
};

