#pragma once
#include "GameObject.h"
#include "Vec2.h"
#include "Factory.h"
#include "Bullet.h"
#include "BulletManager.h"
#include "Mat3.h"
class GalPlayer :
	public GameObject
{
public:
	GalPlayer();
	~GalPlayer();

	void ShipInit();
	bool ShipUpdate(float dt, Engine::Vec2 velocity);
	bool ShipUpdate(float dt);
	bool ShipDraw(Core::Graphics& g);
	void Shoot(float dt);
	bool AmIGonnaDie(float dt);
	Engine::Vec2 ShipPosition();
	int lives{ 5 };


	BulletManager *BM = nullptr;
	Engine::Vec2 m_startingPos{ 400, 550 };
	int m_scale{ 4 };
	Mat3 transform;

	float vel{ 180 };
};

