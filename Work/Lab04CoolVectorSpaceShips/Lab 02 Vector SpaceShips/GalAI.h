#pragma once
#include "GameObject.h"
#include "Vec2.h"
#include "BulletManager.h"
#include "WayPoint.h"
#include "Mat3.h"
class GalAI :
	public GameObject
{
public:
	GalAI(int numShips, int rowNum);
	~GalAI();
	void GalInit();

	void FollowingWayPoints(float dt);

	bool AmIGonnaDie(float dt);

	bool EnemyUpdate(float dt);
	bool EnemyDraw(Core::Graphics& g);
	Vec2 EnemyPosition();
	void ChangeMovement();
	void Shoot(float dt);

	BulletManager *BM = nullptr;
	bool changeTargets{ false };
	int headedTowards{ 0 };
	int changeBy{ 1 };
	int shipNumber{ 1 };
	int row{ 1 };
	int m_scale{ 3 };
	Mat3 transform;
	bool bothWays{ true };
	bool alive{ true };
	bool pathing{ false };
	float pathStartX{ 0 };

	Vec2 m_velocity;
	Engine::Vec2 m_startingPos{ 300, -20 };
	WayPoint wayPoints[2] =
	{ WayPoint(shipNumber, bothWays, row), WayPoint(shipNumber + 1, bothWays, row)};
};

