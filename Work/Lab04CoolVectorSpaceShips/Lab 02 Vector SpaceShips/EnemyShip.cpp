#include "EnemyShip.h"



EnemyShip::EnemyShip()
{
	EnemyInit();
}


EnemyShip::~EnemyShip()
{
}

void EnemyShip::EnemyInit()
{
	shape.ShapeInit(Factory::EnemySpaceShipShape(), Factory::EnemySpaceShipSize(), 1);
	m_pos = m_pos + m_startingPos;
	turret.playerOrEnemy = false;
	turret.origin = shape.m_shape[shape.GetPointsofShape()];
	shape.color1 = 255;
	shape.color2 = 0;
	shape.color3 = 0;
	turret.SetColor(0, 255, 0);
}

void EnemyShip::FollowingWayPoints(float dt)
{
	//std::cout << headedTowards << std::endl;
	Vec2 test = (wayPoints[headedTowards - 1].m_pos - m_pos);
	if (changeTargets == false)
	{
		m_velocity = (wayPoints[headedTowards - 1].m_pos - m_pos) / dt*dt;
		changeTargets = !changeTargets;
	}
	if (test.LengthSquared() < 25)
	{
		changeTargets = !changeTargets;
		if (headedTowards == 6) headedTowards = 1;
		else headedTowards++;
	}
	else if (test.Length() > 1000)
	{
		changeTargets = !changeTargets;
		if (headedTowards == 6) headedTowards = 1;
		else headedTowards++;
	}
	
}


bool EnemyShip::EnemyUpdate(float dt)
{
	turret.endPoint = Vec2(((*turret.lockedOnto).x - m_pos.x), ((*turret.lockedOnto).y - m_pos.y));
	if (parentShip != nullptr)
	{

		Mat3 rotation = Mat3::Rotation(m_headingDegrees * (3.14159/180));
		if (m_headingDegrees > 360) m_headingDegrees - 360;
		m_headingDegrees += 100 * dt * speedOfRot;
		m_pos = parentShip->m_pos + rotation * angVel;
		Mat3 translate = Mat3::Translation(m_pos);
		Mat3 scale = Mat3::Scale((float)m_scale);
		m_transform = translate * scale;	//SRT is the proper order here
	}
	else 
	{
		FollowingWayPoints(dt);
		m_pos = m_pos + (m_velocity * dt);
	}
	static float acumTime = 0;
	acumTime += dt;
	if (acumTime > 0.1f)
	{
		BM->ShootBullet(m_pos, (turret.endPoint.Normalize() * 200));
		acumTime = 0.0f;
	}
	return true;
}

bool EnemyShip::EnemyDraw(Core::Graphics & g)
{
	if (parentShip != nullptr) {
		shape.DrawShape(g, m_transform);
	}
	else {
		shape.DrawShape(g, m_pos, m_scale);
		/*for (int i = 0; i < 6; i++)
		{
			wayPoints[i].WaypointDraw(g);
		}*/
	}
	turret.DrawShape(g, m_pos, shape.m_shape[shape.GetPointsofShape()-1]);
	return true;
}

Vec2 EnemyShip::EnemyPosition()
{
	return Vec2(shape.m_shape[Factory::EnemySpaceShipSize()-1].x, shape.m_shape[Factory::EnemySpaceShipSize()-1].y);
}

void EnemyShip::TurretLockOn(Vec2 * lockedOn)
{
	turret.lockedOnto = lockedOn;
}
