#pragma once
#include "GameObject.h"
#include "Factory.h"
#include "Turret.h"
#include "BulletManager.h"
#include "Bullet.h"

class SpaceShip :
	public GameObject
{
public:
	SpaceShip();
	~SpaceShip();

	void ShipInit();

	bool ShipUpdate(float dt, Engine::Vec2 velocity);
	bool ShipUpdate(float dt);
	bool ShipDraw(Core::Graphics& g);
	Engine::Vec2 ShipPosition();

	Engine::Vec2* ShipPositionPointer();
	Turret m_turret;
	void TurretLockOn(Engine::Vec2* lockedOn);
	Engine::Mat3 m_transform;
	BulletManager *BM = nullptr;

private:
	Engine::Vec2 m_startingPos{ 400, 425 };
	float m_headingDegrees{ 0 };
	float m_turnSpeed{ -5 };
	Engine::Vec2 m_velocity{ 0,0 };
	float m_acceleration{ 80 };
	int m_scale{ 4 };


};

