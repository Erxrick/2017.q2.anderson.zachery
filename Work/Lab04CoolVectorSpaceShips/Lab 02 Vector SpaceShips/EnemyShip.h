#pragma once
#include "GameObject.h"
#include "Factory.h"
#include "WayPoint.h"
#include "Turret.h"
#include "BulletManager.h"
#include "Bullet.h"
class EnemyShip :
	public GameObject
{
public:
	EnemyShip();
	~EnemyShip();

	void EnemyInit();

	void FollowingWayPoints(float dt);
	

	bool EnemyUpdate(float dt);
	bool EnemyDraw(Core::Graphics& g);
	Vec2 EnemyPosition();
	void TurretLockOn(Vec2* lockedOn);
	BulletManager *BM = nullptr;
	EnemyShip *parentShip = nullptr;
	Vec2 angVel{ 0.0f, 40.0f };
	float speedOfRot{ 1 };
	Mat3 m_transform;
	float m_headingDegrees{ 0 };
	int m_scale{ 10 };

private:

	Turret turret;
	Vec2 m_velocity;
	int headedTowards{ 1 };
	bool changeTargets{ false };
	Engine::Vec2 m_startingPos{ 200, 200 };
	WayPoint wayPoints[6] =
	{ WayPoint(1, false, 0), WayPoint(2, false, 0), WayPoint(3, false, 0),
	WayPoint(4, false, 0), WayPoint(5, false, 0), WayPoint(6, false, 0) };
	

};

