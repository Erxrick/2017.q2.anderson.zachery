#include "SpaceShip.h"
#include "Input.h"
#include "Mat3.h"



SpaceShip::SpaceShip()
{
	ShipInit();
}


SpaceShip::~SpaceShip()
{

}

void SpaceShip::ShipInit()
{
 	shape.ShapeInit(Factory::SpaceShipShape(), Factory::SpaceShipSize(), 1);
	m_pos = m_pos + m_startingPos;
	m_turret.playerOrEnemy = true;
	m_turret.origin = m_pos;
	shape.color1 = 255;
	shape.color2 = 0;
	shape.color3 = 255;
	m_turret.SetColor(0,255,0);
}

bool SpaceShip::ShipUpdate(float dt, Engine::Vec2 velocity)
{
	m_pos = m_pos + (velocity * dt);
	m_turret.endPoint = Vec2(((*m_turret.lockedOnto).x - m_pos.x), ((*m_turret.lockedOnto).y - m_pos.y));
	return true;
}

bool SpaceShip::ShipUpdate(float dt)
{
	int forward = 0;
	int turn = 0;
	if (Core::Input::IsPressed('W')) ++forward;
	if (Core::Input::IsPressed('S')) --forward;
	if (Core::Input::IsPressed('A')) --turn;
	if (Core::Input::IsPressed('D')) ++turn;

	if (turn)
	{
		m_headingDegrees += m_turnSpeed * dt * turn;
	}
	Mat3 rotation = Mat3::Rotation(m_headingDegrees);

	if (forward)
	{
		Vec2 direction = Vec2(0.0f, forward + 0.0f);
		direction = rotation.Mult(direction);
		m_velocity = m_velocity + direction * dt * m_acceleration;
	}
	float m_dragCoef = 0.4f;
	m_velocity = m_velocity * (1.0f - dt * m_dragCoef);
	m_pos = m_pos + (m_velocity * dt);
	Mat3 translation = Mat3::Translation(m_pos);
	Mat3 scale = Mat3::Scale((float)m_scale);
	m_transform = translation * (rotation * scale);
	m_turret.endPoint = Vec2(((*m_turret.lockedOnto).x - m_pos.x), ((*m_turret.lockedOnto).y - m_pos.y));

	static float acumTime = 0;
	acumTime += dt;
	if (acumTime > 0.1f)
	{
		BM->ShootBullet(m_pos, (m_turret.endPoint.Normalize() * 200));
		acumTime = 0.0f;
	}
	return false;
}

bool SpaceShip::ShipDraw(Core::Graphics & g)
{
	shape.DrawShape(g, m_transform);
	m_turret.DrawShape(g, m_pos, shape.m_shape[shape.GetPointsofShape() - 1]);
	return true;
}

Vec2 SpaceShip::ShipPosition()
{
	return m_pos;
}
Vec2* SpaceShip::ShipPositionPointer()
{
	Vec2* position = &m_pos;
	return position;
}

void SpaceShip::TurretLockOn(Vec2 * lockedOn)
{
	m_turret.lockedOnto = lockedOn;
}
