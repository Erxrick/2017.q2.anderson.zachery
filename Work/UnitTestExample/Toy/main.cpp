#include <iostream>

#include "Toy.h"

int main() 
{
	printf("Hello, Toys!\n");

	Toy toy;
	int i = 17;
	int j = toy.Doubler(i);
	printf("Doubler(%d) = %d\n", i, j);

	int p = 34;
	int g = toy.Tripler(p);
	printf("Tripler(%d) = %d\n", p, g);

	return 0;
}