#include "stdafx.h"
#include "CppUnitTest.h"
#include "..//Toy//Toy.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ToyTester
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestDoubler)
		{
			Toy toy;
			int i = 77;
			int k = toy.Doubler(i);
			Assert::AreEqual(k, i * 2);
			for (int j = -100; j < 100; ++j)
			{
				Assert::AreEqual(k, i * 2);
			}
		}
		TEST_METHOD(TestTripler)
		{
			Toy toy;
			int i = 14;
			int k = toy.Tripler(i);
			Assert::AreEqual(k, i *3);
		}
		TEST_METHOD(TestGetCurrentID)
		{
			Toy toy;
			int actualID = toy.m_id;
			int currID = toy.GetCurrentID();
			Assert::AreEqual(actualID, currID);
		}

	};
}