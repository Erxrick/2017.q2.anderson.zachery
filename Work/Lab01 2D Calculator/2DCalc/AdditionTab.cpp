#include "AdditionTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;
namespace
{
	Vec2 left;
	Vec2 right;
	Vec2 result;
	void AdditionTabCallBack(const BasicVectorEquationInfo& data)
	{

		left = Vec2(data.x1, data.y1);
		left = left * data.scalar1;
		right = Vec2(data.x2, data.y2);
		right = right * data.scalar2;
		result = (data.add ? (left + right) : (left - right));
		if (data.add == true) {
			std::cout << left << " + " << right << " = " << result << std::endl;
		}
		else {
			std::cout << left << " - " << right << " = " << result << std::endl;
		}
	}
}

AdditionTab::AdditionTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


AdditionTab::~AdditionTab()
{
	Shutdown();
}

bool AdditionTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setBasicVectorEquationData(AdditionTabCallBack, left.Pos(), right.Pos(), result.Pos());
	return false;
}

bool AdditionTab::Shutdown()
{
	return false;
}

