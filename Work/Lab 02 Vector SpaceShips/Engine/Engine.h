#pragma once
#include "ExportHeader.h"

namespace Engine
{
	ENGINE_SHARED bool Initialize();
	ENGINE_SHARED bool Shutdown();

}