#include "Vec2.h"
#include <assert.h>
#include <limits>

namespace Engine
{
	std::ostream & Vec2::Output(std::ostream & os) const
	{
		return os << "[" << x << ", " << y << "]";
	}
	Vec2 Engine::Vec2::operator+(const Vec2 & right) const
	{
		return Vec2(x+right.x, y+right.y);
	}
	Vec2 Vec2::operator-(const Vec2 & right) const
	{
		return Vec2(x - right.x, y - right.y);
	}
	bool Vec2::operator==(const Vec2 & right) const
	{
		bool returnValue = false;
		returnValue =  (CompareFloats(x, right.x) && CompareFloats(y, right.y)) ?  true : false;
		return returnValue;
	}
	Vec2 Vec2::operator+() const
	{
		return Vec2(x, y);
	}
	Vec2 Vec2::operator-() const
	{
		return Vec2(x*-1, y*-1);
	}
	Vec2 Vec2::operator*(float scaler) const
	{
		return Vec2(x*scaler, y*scaler);
	}
	float Vec2::operator*(const Vec2& right) const
	{
		return ((x*right.x) + (y*right.y));
	}
	Vec2 Vec2::operator/(float scaler) const
	{
		return Vec2(x/scaler, y/scaler);
	}
	float Vec2::Length() const
	{
		return sqrt(LengthSquared());
	}
	float Vec2::LengthSquared() const
	{
		return ((x*x) + (y*y));
	}
	Vec2 Vec2::Normalize() const
	{
		float magnitude = Length();
		assert(magnitude != 0);
		return Vec2(x / magnitude, y / magnitude);
	}
	Vec2 Vec2::PerpCW() const
	{
		return Vec2(-y, x);
	}
	Vec2 Vec2::PerpCCW() const
	{
		return Vec2(y, -x);
	}

	float Vec2::Dot(const Vec2 & left, const Vec2 & right) const
	{
		return left * right;
	}
	float Vec2::Cross(const Vec2& left, const Vec2& right) const
	{
		return ((left.x*right.y) - (left.y*right.x));
	}
	bool Vec2::CompareFloats(float a, float b)
	{
			float range = 0.00001f;
			float diff = 0.0f;
			if (a > b)
			{
				diff = a - b;
			}
			else
			{
				diff = b - a;
			}
			return (range > diff);
	}
}
