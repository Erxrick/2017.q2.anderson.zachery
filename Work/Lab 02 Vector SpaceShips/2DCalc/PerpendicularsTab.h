#pragma once

class RenderUI;
class PerpendicularsTab
{
public:
	PerpendicularsTab(RenderUI* pRenderUI);
	~PerpendicularsTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};
