#include "LerpTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;
namespace
{
	Vec2 a;
	Vec2 b;
	Vec2 aMinusB;
	Vec2 aLerpPortion;
	Vec2 bLerpPortion;
	Vec2 lerpResult;

	void LerpTabCallback(const LerpData& data)
	{
		a = Vec2(data.a_i, data.a_j);
		b = Vec2(data.b_i, data.b_j);
		aMinusB = b-a;
		aLerpPortion = (1 - data.beta)*a;
		bLerpPortion = data.beta*b;
		lerpResult = Lerp(a, b, data.beta);
	}
}

LerpTab::LerpTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


LerpTab::~LerpTab()
{
	Shutdown();
}

bool LerpTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setLerpData(a.Pos(), b.Pos(), aMinusB.Pos(), aLerpPortion.Pos(), bLerpPortion.Pos(), lerpResult.Pos(), LerpTabCallback);
	return false;
}

bool LerpTab::Shutdown()
{
	return false;
}

