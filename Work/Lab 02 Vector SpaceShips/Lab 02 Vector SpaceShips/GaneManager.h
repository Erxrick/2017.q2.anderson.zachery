#include "Core.h"
#include "Input.h"
#include "graphics.h"
#include "SpaceShip.h"
#include "Wrapper.h"
#include "Input.h"
#include "EnemyShip.h"
#include "Wall.h"
#pragma once

class GaneManager
{
public:
	GaneManager(int width = 800, int height = 600);
	~GaneManager();

	bool IsInitialized() const { return m_isInitialized; };
	bool RunGame();

	void Drawer(Core::Graphics& g);
	bool Updater(float dt);
	
	static void Draw(Core::Graphics& g);
	static bool Update(float dt);
	Vec2* getMousePosition() { return &mousePosition; };

	SpaceShip playerShip;
	EnemyShip enemyShip;

	Wall *wall1;
	Wall *wall2;
	Wall *wall3;
private: 
	bool Initialize();
	bool Shutdown();

	void Wrapper();
	void Bounce();

	void FPS(float dt);

	Factory fac;
	Core::Input m_input;
	Vec2 buttonMovement();
	Vec2 mousePosition{ 0.0f, -10.0f };

	bool m_isInitialized{ false };
	int m_screenWidth;
	int m_screenHeight;
	int m_fps;
	bool m_switch{ false };

	//testing
	Vec2 defaultVelocity{ 45.0f,45.0f };
	

private:
};

