#include "PerpendicularsTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;
namespace
{
	Vec2 original;
	Vec2 normal;
	Vec2 cwPerpendicular;
	Vec2 ccwPerpendicular;

	void PerpendicularDataCallBack(const PerpendicularData& data)
	{

		original = Vec2(data.x, data.y);
		normal = original.Normalize();
		cwPerpendicular = original.PerpCW();
		ccwPerpendicular = original.PerpCCW();
	}
}

PerpendicularsTab::PerpendicularsTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


PerpendicularsTab::~PerpendicularsTab()
{
	Shutdown();
}

bool PerpendicularsTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setPerpendicularData(original.Pos(), normal.Pos(), cwPerpendicular.Pos(), ccwPerpendicular.Pos(), PerpendicularDataCallBack);
	return false;
}

bool PerpendicularsTab::Shutdown()
{
	return false;
}