#pragma once

class RenderUI;
class AffineTranformTab
{
public:
	AffineTranformTab(RenderUI* pRenderUI);
	~AffineTranformTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};

