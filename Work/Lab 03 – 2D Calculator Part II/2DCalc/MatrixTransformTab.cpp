
#include "MatrixTransformTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include "Mat3.h"

using namespace Engine;
namespace
{
	Mat3 transform;
	Mat3 multTransforms[100];
	Vec2 ship[] = { { -5,-4 },{ -5,1 },{ -5,1 },{ -4,1 },{ -4,1 },{ -4,-1 },{ -4,-1 },{ -3,-1 },{ -3,-1 },{ -3,4 },{ -3,4 },{ -2,4 },{ -2,4 },{ -2,2 },{ -2,2 },{ -1,2 },{ -1,2 },{ -1,4 },{ -1,4 },{ 0,4 },{ 0,4 },{ 0,7 },{ 0,7 },{ 1,7 },{ 1,7 },{ 1,4 },{ 1,4 },{ 2,4 },{ 2,4 },{ 2,2 },{ 2,2 },{ 3,2 },{ 3,2 },{ 3,4 },{ 3,4 },{ 4,4 },{ 4,4 },{ 4,-1 },{ 4,-1 },{ 5,-1 },{ 5,-1 },{ 5,1 },{ 5,1 },{ 6,1 },{ 6,1 },{ 6,-4 },{ 6,-4 },{ 5,-4 },{ 5,-4 },{ 5,-3 },{ 5,-3 },{ 4,-3 },{ 4,-3 },{ 4,-2 },{ 4,-2 },{ 3,-2 },{ 3,-2 },{ 3,-3 },{ 3,-3 },{ 1,-3 },{ 1,-3 },{ 1,-4 },{ 1,-4 },{ 0,-4 },{ 0,-4 },{ 0,-3 },{ 0,-3 },{ -2,-3 },{ -2,-3 },{ -2,-2 },{ -2,-2 },{ -3,-2 },{ -3,-2 },{ -3,-3 },{ -3,-3 },{ -4,-3 },{ -4,-3 },{ -4,-4 },{ -4,-4 },{ -5,-4 } };
	Vec2 spareShip[80];
	int linecount = 40;
	void Set2DMatrixVerticesTransformData(const MatrixTransformData2D& data)
	{
		multTransforms[data.selectedMatrix] = Mat3::Translation(Vec2(data.translateX, data.translateY)) * Mat3::Rotation(data.rotate) * Mat3::ScaleY(data.scaleY)  * Mat3::ScaleX(data.scaleX) ;
		transform = Mat3();
		for (int i = 0; i < 100; i++)
		{
		transform = transform * multTransforms[i];
		}
		for (int i = 0; i < 80; i++)
		{
			spareShip[i] =  multTransforms[data.selectedMatrix] * ship[i];
		}
	}
}

MatrixTransformTab::MatrixTransformTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


MatrixTransformTab::~MatrixTransformTab()
{
	Shutdown();
}

bool MatrixTransformTab::Initialize(RenderUI * pRenderUI)
{
	for (int i = 0; i < 80; i++)
	{
		ship[i] = ship[i] / 10;
	}
	pRenderUI->set2DMatrixVerticesTransformData(spareShip->Pos(), linecount, multTransforms->Pos(), transform.Pos(), Set2DMatrixVerticesTransformData);
	return false;
}

bool MatrixTransformTab::Shutdown()
{
	return false;
}

