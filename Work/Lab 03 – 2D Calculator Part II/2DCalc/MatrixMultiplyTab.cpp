#include "MatrixMultiplyTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include "Mat2.h"

using namespace Engine;
namespace
{
	Vec2 altered;
	Vec2 result;
	Mat2 transformation;
	void LinearTranformationCallback(const LinearTransformationData& data)
	{
		transformation = Mat2(Vec2(data.m00, data.m10), Vec2(data.m01, data.m11));
		altered = Vec2(data.v0, data.v1);
		result = transformation * altered;
		
	}
}

MatrixMultiplyTab::MatrixMultiplyTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


MatrixMultiplyTab::~MatrixMultiplyTab()
{
	Shutdown();
}

bool MatrixMultiplyTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setLinearTransformationData(result.Pos(), LinearTranformationCallback);
	return false;
}

bool MatrixMultiplyTab::Shutdown()
{
	return false;
}

