#include "AffineTranformTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include "Vec3.h"
#include "Mat3.h"

using namespace Engine;
namespace
{
	Mat3 matrix;
	Vec3 vecs[5];
	Vec3 resultVecs[5];
	void SetAffineTransformationData(const AffineTransformationData& data)
	{
		matrix = Mat3(data.data[0], data.data[1], data.data[2],
			data.data[3], data.data[4], data.data[5],
			data.data[6], data.data[7], data.data[8]);
		int iterator = 0;
		for (int i = 9; i < 24; i += 3)
		{
			vecs[iterator] = Vec3(data.data[i], data.data[i + 1], data.data[i + 2]);
			iterator++;
		}
		for (int i = 0; i < 5; i++)
		{
			resultVecs[i] = matrix * vecs[i];
		}
	}
}

AffineTranformTab::AffineTranformTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


AffineTranformTab::~AffineTranformTab()
{
	Shutdown();
}

bool AffineTranformTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setAffineTransformationData(resultVecs->Pos(), SetAffineTransformationData);
	return false;
}

bool AffineTranformTab::Shutdown()
{
	return false;
}

