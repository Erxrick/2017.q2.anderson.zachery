#include <iostream>
#include "RenderUI.h"
#include "TabManager.h"
#include "Vec2.h"
#include "Engine.h"

using namespace Engine;

int main(int argc, char** argv)
{
	if (!Engine::Initialize()) return -2;

	RenderUI renderUI;
	TabManager tabs(&renderUI);


	if (!renderUI.initialize(argc, argv)) return -1;
	renderUI.run();
	renderUI.shutdown();

	return 0;
}