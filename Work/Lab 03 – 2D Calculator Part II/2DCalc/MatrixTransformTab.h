#pragma once

class RenderUI;
class MatrixTransformTab
{
public:
	MatrixTransformTab(RenderUI* pRenderUI);
	~MatrixTransformTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};
