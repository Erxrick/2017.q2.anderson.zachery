#include "CppUnitTest.h"
#include "..//Engine/Mat2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<>
			std::wstring ToString<Mat2>(const Mat2 &mat2) {
				return (L"" + std::to_wstring(mat2.matrixArray[0])) + (L", " + std::to_wstring(mat2.matrixArray[1])) +
					(L", " + std::to_wstring(mat2.matrixArray[2])) + (L", " + std::to_wstring(mat2.matrixArray[3]));
			}
			template<>
			std::wstring ToString<Vec2>(const Vec2 &vec2) {
				return (L"x = " + std::to_wstring(vec2.x) + L" y = " + std::to_wstring(vec2.y));
			}
		}
	}
}

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Mat2Test
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(NewMat2Identity)
		{
			Mat2 identity = Mat2();
			Mat2 realIdentity = Mat2(Vec2(1, 0), Vec2(0, 1));
			Assert::AreEqual(identity, realIdentity, L"The default constructor did not give an identity matrix");
		}
		TEST_METHOD(RotationTest)
		{
			Mat2 rotated = Mat2::Rotate(45);

			Mat2 manualRotation = Mat2(Vec2(cos(-45), sin(-45)),Vec2(-sin(-45), cos(-45)));

			Assert::AreEqual(rotated, manualRotation, L"The rotation was not completed properly");
		}
		TEST_METHOD(ScaleMat2)
		{
			Mat2 scaleFloat = Mat2::Scale(7);
			Mat2 manScaleFloat = Mat2(Vec2(7, 0), Vec2(0, 7));
			Assert::AreEqual(scaleFloat, manScaleFloat, L"Symm uniform scaling is broken");

			Mat2 scaleFloat2 = Mat2::Scale(7, 3);
			Mat2 manScaleFloat2 = Mat2(Vec2(7, 0), Vec2(0, 3));
			Assert::AreEqual(scaleFloat2, manScaleFloat2, L"Nonuniform 2-var scaling is broken");

			Mat2 scaleFloat3 = Mat2::Scale(Vec2(3, 4));
			Mat2 manScaleFloat3 = Mat2(Vec2(3, 0), Vec2(0, 4));
			Assert::AreEqual(scaleFloat3, manScaleFloat3, L"Vector scaling is broken");

			Mat2 scaleFloat4 = Mat2::ScaleX(8) * Mat2::ScaleY(9);
			Mat2 manScaleFloat4 = Mat2(Vec2(8, 0), Vec2(0, 9));
			Assert::AreEqual(scaleFloat4, manScaleFloat4, L"Symm scaling is broken");
		}
		TEST_METHOD(MultiplicationTest)
		{
			Mat2 mult1 = Mat2(Vec2(2, 0), Vec2(2, 1));
			Mat2 mult2 = Mat2(Vec2(3, 0), Vec2(3, 7));

			Mat2 result = mult1 * mult2;
			Mat2 actualResult = Mat2(Vec2(6,0), Vec2(20, 7));
			Assert::AreEqual(result, actualResult, L"The Multiplication between to mat3s broke");
	
			Mat2 mult3 = Mat2(Vec2(2, 2), Vec2(2, 2));
			Mat2 result2 = mult3 * 3;
			Assert::AreEqual(result2, Mat2(Vec2(6, 6), Vec2(6, 6)), L"This multiplication for scaling by a float failed");
		
		}

	};
}