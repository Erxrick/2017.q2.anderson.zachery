#include "SpaceShip.h"



SpaceShip::SpaceShip()
{
	ShipInit();
}


SpaceShip::~SpaceShip()
{

}

void SpaceShip::ShipInit()
{
 	shape.ShapeInit(Factory::SpaceShipShape(), Factory::SpaceShipSize(), 4);
	m_pos = m_pos + m_startingPos;
	m_turret.playerOrEnemy = true;
	m_turret.origin = m_pos;
}

bool SpaceShip::ShipUpdate(float dt, Engine::Vec2 velocity)
{
	m_turret.endPoint = Vec2(((*m_turret.lockedOnto).x - m_pos.x), ((*m_turret.lockedOnto).y - m_pos.y));
	m_pos = m_pos + (velocity * dt);
	//this->UpdateObj(dt, velocity);
	return true;
}

bool SpaceShip::ShipDraw(Core::Graphics & g)
{
	shape.DrawShape(g, m_pos);
	m_turret.DrawShape(g, m_pos, shape.m_shape[shape.GetPointsofShape() - 1]);
	return true;
}

Vec2 SpaceShip::ShipPosition()
{
	return m_pos;
}
Vec2* SpaceShip::ShipPositionPointer()
{
	Vec2* position = &m_pos;
	return position;
}

void SpaceShip::TurretLockOn(Vec2 * lockedOn)
{
	m_turret.lockedOnto = lockedOn;
}
