#pragma once
#include "GameObject.h"
#include "Factory.h"
#include "WayPoint.h"
#include "Turret.h"
class EnemyShip :
	public GameObject
{
public:
	EnemyShip();
	~EnemyShip();

	void EnemyInit();

	void FollowingWayPoints(float dto);
	

	bool EnemyUpdate(float dt);
	bool EnemyDraw(Core::Graphics& g);
	Vec2 EnemyPosition();
	void TurretLockOn(Vec2* lockedOn);

private:

	Turret turret;
	Vec2 m_velocity;
	int headedTowards{ 1 };
	bool changeTargets{ false };
	Engine::Vec2 m_startingPos{ 200, 200 };
	WayPoint wayPoints[6] =
	{ WayPoint(1), WayPoint(2), WayPoint(3),
	WayPoint(4), WayPoint(5), WayPoint(6) };
	

};

