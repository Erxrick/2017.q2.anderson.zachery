#include "Shape.h"


Shape::Shape()
{

}

Shape::~Shape()
{
}




void Shape::DrawShape(Core::Graphics & g, const Engine::Vec2& position)
{
	g.SetColor(RGB(255, 0, 0));
	for (int i = 0; i < m_numShapePoints-2; i++)
	{
		g.DrawLine(m_shape[i].x + position.x, m_shape[i].y + position.y, m_shape[i+1].x + position.x, m_shape[i+1].y + position.y);
	}
//	g.DrawLines((m_numShapePoints-1) / 2, m_shape[0].Pos());
}

bool Shape::ShapeInit(Engine::Vec2* shape, int numPoints, int scaler)
{
	m_numShapePoints = numPoints;
	m_shape = shape;
	for (int j = 0; j < m_numShapePoints; ++j)
	{
		m_shape[j] = (m_shape[j] * scaler);
	}
	return true;
}
