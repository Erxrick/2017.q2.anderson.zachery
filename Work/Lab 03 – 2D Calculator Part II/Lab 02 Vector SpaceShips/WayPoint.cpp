#include "WayPoint.h"



WayPoint::WayPoint(int num)
{
	Init(num);
}


WayPoint::~WayPoint()
{
}

bool WayPoint::WaypointDraw(Core::Graphics g)
{
	shape.DrawShape(g, m_pos);
	return true;
}

void WayPoint::Init(int num)
{
	order = num;
	shape.ShapeInit(Factory::WaypointShape(), Factory::WaypointSize(), 1);
	m_pos = m_pos + Vec2(rand() % 800, rand() % 600);
}
