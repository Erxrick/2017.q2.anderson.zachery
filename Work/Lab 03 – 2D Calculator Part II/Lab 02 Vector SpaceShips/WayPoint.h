#pragma once
#include "Shape.h"
#include "Factory.h"
#include "GameObject.h"

class WayPoint :
	public GameObject
{
public:
	WayPoint(int num);
	~WayPoint();

	bool WaypointDraw(Core::Graphics g);

	Shape shape;
	int order;
private:
	void Init(int num);
};

