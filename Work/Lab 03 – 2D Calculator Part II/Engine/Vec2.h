#pragma once
#include <iostream>
#include "ExportHeader.h"
#include <assert.h>
#include "Engine.h"


namespace Engine 
{
	//vec2 is an immutable vector class, except for assignment
	class ENGINE_SHARED Vec2
	{
	public:
		Vec2()						: Vec2(0.0f, 0.0f) {};
		Vec2(float xx, float yy)	: x(xx), y(yy) {};


		std::ostream& Output(std::ostream& os) const;
		Vec2 operator+(const Vec2& right) const;
		Vec2 operator-(const Vec2& right) const;
		bool operator==(const Vec2& right) const;
		Vec2 operator+() const;
		Vec2 operator-() const;
		Vec2 operator*(float scaler) const;
		float operator*(const Vec2& right) const;
		Vec2 operator/(float scaler) const;

		float Length() const;
		float LengthSquared() const;
		Vec2 Normalize() const;
		Vec2 PerpCW() const;
		Vec2 PerpCCW() const;
		float Dot(const Vec2& left, const Vec2& right) const;
		float Cross(const Vec2& left, const Vec2& right) const;
		float* Pos() { return &x; };
			
	public:
		float x, y;

	private:
		static bool CompareFloats(float a, float b);
	};

	inline Vec2 operator*(float scaler, const Vec2& v)
	{
		return v * scaler;
	}

	inline std::ostream& operator<<(std::ostream& os, const Vec2& v)
	{
		return v.Output(os);
	}
	inline Vec2 Lerp(Vec2 v1, Vec2 v2, float t)
	{
		assert(t >= 0);
		assert(t <= 1);
		Vec2 aLerpPortion = (1 - t)*v1;
		Vec2 bLerpPortion = t*v2;
		return aLerpPortion + bLerpPortion;
	}
	inline float Dot(const Vec2& left,const Vec2& right)
	{
		return left * right;
	}
	inline float Cross(const Vec2& left, const Vec2& right)
	{
		//test this bitch heavily. idk if its right	
		return ((left.x*right.y) - (left.y*right.x));
	}
}

