#pragma once
#include "Vec2.h"
#include"ExportHeader.h"
namespace Engine {

	class Vec3
	{
	public:
		Vec3() : x(0), y(0), z(0) {};
		Vec3(float x, float y, float z) : x(x), y(y), z(z) {};
		Vec3(Vec2 vec2, float z) : x(vec2.x), y(vec2.y), z(z) {};
		float* Pos() { return &x; };



		float x, y, z;
	};

}
