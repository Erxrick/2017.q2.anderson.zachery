#pragma once
#include "ExportHeader.h"
#include <iostream>
#include "Vec2.h"
#include "Vec3.h"
#include "Engine.h"


namespace Engine
{
	class ENGINE_SHARED Mat3
	{
	public:
		Mat3() : Mat3(Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f)) {};
		Mat3(Vec2 col1, Vec2 col2, Vec2 col3 = Vec2(0,0)) 
		{
			matrixArray[0] = col1.x;
			matrixArray[1] = col2.x;
			matrixArray[2] = col3.x;
			matrixArray[3] = col1.y;
			matrixArray[4] = col2.y;
			matrixArray[5] = col3.y;
			matrixArray[6] = 0.0f;
			matrixArray[7] = 0.0f;
			matrixArray[8] = 1;
		};
		Mat3(float p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8)
		{
			matrixArray[0] = p0;
			matrixArray[1] = p1;
			matrixArray[2] = p2;
			matrixArray[3] = p3;
			matrixArray[4] = p4;
			matrixArray[5] = p5;
			matrixArray[6] = p6;
			matrixArray[7] = p7;
			matrixArray[8] = p8;
		}

		static Mat3 Rotation(float degrees);
		static Mat3 Scale(float scale);
		static Mat3 Scale(float scaleX, float scaleY);
		static Mat3 Scale(Vec2 scale);
		static Mat3 ScaleX(float scale);
		static Mat3 ScaleY(float scale);
		static Mat3 Translation(float x, float y);
		static Mat3 Translation(Vec2& vec);

		Mat3 operator*(Mat3& matrix3) const;
		Vec2 operator*(Vec2& vec) const;
		Vec3 operator*(Vec3& vec) const;
		Vec2 Mult(Vec2& vec) const;
		bool operator==(const Mat3& mat3) const;


		std::ostream& Output(std::ostream& os) const;
		float* Pos() { return &matrixArray[0]; };



		float matrixArray[9];

	private:
		static bool CompareFloats(float a, float b);

	};
	inline std::ostream& operator<<(std::ostream& os, const Mat3& v)
	{
		return v.Output(os);
	}
}

