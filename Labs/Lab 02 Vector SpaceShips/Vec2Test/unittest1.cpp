#include "CppUnitTest.h"
#include "Vec2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<>
			std::wstring ToString<Vec2>(const Vec2 &vec2) {
				return (L"x = " + std::to_wstring(vec2.x) + L" y = " + std::to_wstring(vec2.y));
			}
		}
	}
}

namespace Vec2Test
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestVec2EqualOverloader)
		{
			Vec2 left(2.0f, 2.0f);
			Vec2 right(2.0f, 2.0f);
			bool result = (left == right);
			Assert::AreEqual(true, result, L"The == overload is broken");
		}
		TEST_METHOD(TestVec2Addition)
		{
			Vec2 left(1.5f, 1.6f);
			Vec2 right(2.5f, 0.6f);
			Vec2 result = left + right;

			Vec2 num1(1.3f, 1.3f);
			Vec2 num2(1.3f, 1.3f);
			Vec2 num3(1.3f, 1.3f);
			Vec2 num4(1.3f, 1.3f);
			Vec2 num5(1.3f, 1.3f);
			Vec2 result2 = num1 + num2 + num3 + num4 + num5;



			Assert::AreEqual(Vec2(4.0f, 2.2f), result, L"Addition broke");
			Assert::AreEqual(Vec2(6.5f, 6.5f), result2, L"Addition of multiple Vectors failed");
		}

		TEST_METHOD(TestVec2Subtraction)
		{
			Vec2 left(1.5f, 9.5f);
			Vec2 right(0.5f, 0.5f);
			Vec2 result = left - right;

			Vec2 leg1(0.0f, 4.0f);
			Vec2 leg2(3.0f, 0.0f);
			Vec2 hypot = leg1 - leg2;
			

			Assert::AreEqual(Vec2(1.0f, 9.0f), result, L"Subtraction broke");
			Assert::AreEqual(Vec2(-3.0f, 4.0f), hypot, L"Did not recieve the hypotenuse of the triangle");
			Assert::AreEqual(5.0f, hypot.Length(), L"Did not recieve the length of the hypotenuse of the triangle");
		}
		
		TEST_METHOD(TestVec2MultiplicationByVectors)
		{
			Vec2 left(1.5f, 1.6f);
			Vec2 right(2.5f, 0.6f);
			float result = left * right;

			Vec2 perp1(0.0f, -1.0f);
			Vec2 perp2(1.0f, 0.0f);
			float resultOfPerp = perp1 * perp2;

			Vec2 num1(3.0f, 0.0f);
			Vec2 num2(5.0f, 5.0f);
			float result3 = Dot(num1, num2);
			float result4 = float(num1.Length() * num2.Length() * cos(45 * (3.14159265359 / 180)));

			Vec2 v1(1.6f, 5.8f);
			Vec2 v2(0.0f, 9.4f);

			Vec2 projection = (v1 * v2.Normalize()) * v2.Normalize();


			Assert::AreEqual(4.71f, result, L"Multiplication failed");
			Assert::AreEqual(0.0f, resultOfPerp, L"Its not return 0 when multiplying 2 perpendicular vectors");
			Assert::AreEqual(result3, result4, L" the dot product of two vectors with an angle of 45� between them results in the product of the lengths of the two vectors times cos(45�)");
			Assert::AreEqual(projection, Vec2(0.0f, 5.8f), L"Projection failed");
		}

		TEST_METHOD(TestVec2MultiplicationByScalar)
		{
			Vec2 left(1.5f, 1.6f);
			float right = 2.2f;
			Vec2 result = left * right;	
			Assert::AreEqual(Vec2(3.3f, 3.52f), result, L"Vec2 multiplied incorrectly");
		}

		TEST_METHOD(TestVec2Division)
		{
			Vec2 left(1.5f, 2.5f);
			float right = .5f;
			Vec2 result = left / right;
			Assert::AreEqual(Vec2(3.0f, 5.0f), result, L"Vec2 divided Improperly");
		}
		TEST_METHOD(TestVec2GettingLengthorMagnitude)
		{
			Vec2 left(3.0f, 4.0f);
			float magnitude = left.Length();
			float magSquared = left.LengthSquared();

			Assert::AreEqual(5.0f, magnitude, L"The magnitude was not gathered properly");
			Assert::AreEqual(25.0f, magSquared, L"The magnitude squared was not gathered properly");
		}
		TEST_METHOD(TestVec2NormalizingAVector)
		{
			Vec2 left(3.0f, 3.0f);
			Vec2 result = left.Normalize();
			float lengthResult = result.Length();
			Assert::AreEqual(Vec2(0.707107f, 0.707107f), result, L"Did not Normailze Properly");
			Assert::AreEqual(1.0f, lengthResult, L"Did not Normailze Properly");
		}
		TEST_METHOD(TestVec2Zeros)
		{
			Vec2 zero(0.0f, 0.0f);
			float length = zero.Length();
				//uncomment this to view that this fails
			//Vec2 breaker = zero.Normalize();
			
			Assert::AreEqual(0.0f, length, L"The length test failed with zeros");
		}
		//Lamb said not to worry about implementing or testing Cross Product
	};
}