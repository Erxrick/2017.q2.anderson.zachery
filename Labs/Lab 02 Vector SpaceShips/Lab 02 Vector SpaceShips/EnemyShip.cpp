#include "EnemyShip.h"



EnemyShip::EnemyShip()
{
	EnemyInit();
}


EnemyShip::~EnemyShip()
{
}

void EnemyShip::EnemyInit()
{
	shape.ShapeInit(Factory::EnemySpaceShipShape(), Factory::EnemySpaceShipSize(), 4);
	m_pos = m_pos + m_startingPos;
	turret.playerOrEnemy = false;
	turret.origin = shape.m_shape[shape.GetPointsofShape()];
}

void EnemyShip::FollowingWayPoints(float dt)
{
	//std::cout << headedTowards << std::endl;
	Vec2 test = (wayPoints[headedTowards - 1].m_pos - m_pos);
	if (changeTargets == false)
	{
		m_velocity = (wayPoints[headedTowards - 1].m_pos - m_pos) / dt*dt;
		changeTargets = !changeTargets;
	}
	if (test.LengthSquared() < 25)
	{
		changeTargets = !changeTargets;
		if (headedTowards == 6) headedTowards = 1;
		else headedTowards++;
	}
	else if (test.Length() > 1000)
	{
		changeTargets = !changeTargets;
		if (headedTowards == 6) headedTowards = 1;
		else headedTowards++;
	}
	
}

bool EnemyShip::EnemyUpdate(float dt)
{
	turret.endPoint = Vec2(((*turret.lockedOnto).x - m_pos.x), ((*turret.lockedOnto).y - m_pos.y));
	FollowingWayPoints(dt);
	m_pos = m_pos + (m_velocity * dt);
	return true;
}

bool EnemyShip::EnemyDraw(Core::Graphics & g)
{
	shape.DrawShape(g, m_pos);
	turret.DrawShape(g, m_pos, shape.m_shape[shape.GetPointsofShape()-1]);
	for (int i = 0; i < 6; i++)
	{
		wayPoints[i].WaypointDraw(g);
	}
	return true;
}

Vec2 EnemyShip::EnemyPosition()
{
	return Vec2(shape.m_shape[Factory::EnemySpaceShipSize()-1].x, shape.m_shape[Factory::EnemySpaceShipSize()-1].y);
}

void EnemyShip::TurretLockOn(Vec2 * lockedOn)
{
	turret.lockedOnto = lockedOn;
}
