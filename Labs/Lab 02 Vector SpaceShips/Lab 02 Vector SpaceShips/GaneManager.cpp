#include "GaneManager.h"
#include "Input.h"
#include <string>
#include "Factory.h"


GaneManager *manager = nullptr;

GaneManager::GaneManager(int width, int height) 
	: m_screenWidth(width), m_screenHeight(height)
{
	m_isInitialized = Initialize();
	manager = this;
	wall1 = new Wall(Vec2(rand() % m_screenWidth, rand() % m_screenHeight), Vec2(rand() % m_screenWidth, rand() % m_screenHeight));
	std::cout << "Beginning of update" << std::endl;
	wall2 = new Wall(Vec2(rand() % m_screenWidth, rand() % m_screenHeight), Vec2(rand() % m_screenWidth, rand() % m_screenHeight));
	wall3 = new Wall(Vec2(rand() % m_screenWidth, rand() % m_screenHeight), Vec2(rand() % m_screenWidth, rand() % m_screenHeight));

}


GaneManager::~GaneManager()
{
	delete wall1;
	delete wall2;
	delete wall3;
	Shutdown();
}

bool GaneManager::RunGame()
{
	std::cout << "Before game loop" << std::endl;
	


	Core::GameLoop();
	std::cout << "After game loop" << std::endl;
	return true;
}

void GaneManager::Drawer(Core::Graphics & g)
{
	if (m_switch == true)
	{
		g.DrawString(50, 50, "Wrap Mode");
	}
	else {
		g.DrawString(50, 50, "Bounce Mode");
	}
	g.SetColor(RGB(255, 255, 255));
	g.DrawString(m_screenWidth-50, 50, std::to_string(m_fps).c_str());
	playerShip.ShipDraw(g);
	enemyShip.EnemyDraw(g);
	wall1->Draw(g);
	wall2->Draw(g);
	wall3->Draw(g);

}

void GaneManager::Wrapper()
{
	if (playerShip.ShipPosition().x > (float)m_screenWidth)
	{
		playerShip.ShipUpdate(1.0f, Vec2((-1 * (float)m_screenWidth), 0));
	}
	else if (playerShip.ShipPosition().x < 0)
	{
		playerShip.ShipUpdate(1.0f, Vec2((float)(m_screenWidth), 0));
	}
	else if (playerShip.ShipPosition().y > m_screenHeight)
	{
		playerShip.ShipUpdate(1.0f, Vec2(0, (-1 * (float)m_screenHeight)));
	}
	else if (playerShip.ShipPosition().y < 0)
	{
		playerShip.ShipUpdate(1.0f, Vec2(0, (float)m_screenHeight));
	}
}
void GaneManager::Bounce()
{
	if (playerShip.ShipPosition().x >(float)m_screenWidth)
	{
		playerShip.ShipUpdate(1.0f, Vec2(-5, 0));
	}
	else if (playerShip.ShipPosition().x < 0)
	{
		playerShip.ShipUpdate(1.0f, Vec2(5, 0));
	}
	else if (playerShip.ShipPosition().y > m_screenHeight)
	{
		playerShip.ShipUpdate(1.0f, Vec2(0, -5));
	}
	else if (playerShip.ShipPosition().y < 0)
	{
		playerShip.ShipUpdate(1.0f, Vec2(0, 5));
	}
}
void GaneManager::FPS(float dt)
{
	static float timer;
	if (timer < 0.5f)
	{
		timer = timer + dt;
	}
	else {
		timer = 0;
		m_fps = (int)(1 / dt);
	}
}
bool GaneManager::Updater(float dt)
{

	FPS(dt);
	if (m_input.IsPressed(VK_RBUTTON))
	{
		mousePosition = Vec2(m_input.GetMouseX(), m_input.GetMouseY());
		playerShip.TurretLockOn(getMousePosition());
	}
	if (m_input.IsPressed('T')) m_switch = true;
	if (m_input.IsPressed('F')) m_switch = false;
	(m_switch == true) ? Wrapper() : Bounce();
	playerShip.ShipUpdate(dt, buttonMovement());
	enemyShip.EnemyUpdate(dt);
	wall1->Update(dt, playerShip);
	wall2->Update(dt, playerShip);
	wall3->Update(dt, playerShip);


	return false;
}


Vec2 GaneManager::buttonMovement()
{
	float vel = 80;
	Vec2 returnVec = Vec2(0.0f, 0.0f);
	if (m_input.IsPressed(m_input.KEY_LEFT))
	{
		returnVec = returnVec + Vec2(-vel, 0.0f);
	}
	if (m_input.IsPressed(m_input.KEY_RIGHT))
	{
		returnVec = returnVec + Vec2(vel, 0.0f);
	}
	if (m_input.IsPressed(m_input.KEY_UP))
	{
		returnVec = returnVec + Vec2(0, -vel);
	}
	if (m_input.IsPressed(m_input.KEY_DOWN))
	{
		returnVec = returnVec + Vec2(0.0f, vel);
	}
	return returnVec;
}

bool GaneManager::Shutdown()
{
	Core::Shutdown();
	return true;
}

bool GaneManager::Update(float dt)
{
	manager->Updater(dt);
	return false;
}
void GaneManager::Draw(Core::Graphics& g)
{
	manager->Drawer(g);
}

bool GaneManager::Initialize()
{
	Core::Init("Starship Troopers", m_screenWidth, m_screenHeight, 2500);
	Core::RegisterUpdateFn(Update);
	Core::RegisterDrawFn(Draw);
	enemyShip.TurretLockOn(playerShip.ShipPositionPointer());
	playerShip.TurretLockOn(getMousePosition());
	return true;
}