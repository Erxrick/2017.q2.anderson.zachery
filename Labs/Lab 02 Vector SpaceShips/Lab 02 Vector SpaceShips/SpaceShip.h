#pragma once
#include "GameObject.h"
#include "Factory.h"
#include "Turret.h"

class SpaceShip :
	public GameObject
{
public:
	SpaceShip();
	~SpaceShip();

	void ShipInit();

	bool ShipUpdate(float dt, Engine::Vec2 velocity);
	bool ShipDraw(Core::Graphics& g);
	Engine::Vec2 ShipPosition();

	Engine::Vec2* ShipPositionPointer();
	Turret m_turret;
	void TurretLockOn(Engine::Vec2* lockedOn);

private:
	Engine::Vec2 m_startingPos{ 400, 425 };

};

