#pragma once

class RenderUI;
class AdditionTab
{
public:
	AdditionTab(RenderUI* pRenderUI);
	~AdditionTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};
