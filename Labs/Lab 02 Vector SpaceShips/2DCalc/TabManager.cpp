#include "TabManager.h"
#include "RenderUI.h"
#include "AdditionTab.h"
#include "PerpendicularsTab.h"
#include "DotProductTab.h"
#include "LerpTab.h"


TabManager::TabManager(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


TabManager::~TabManager()
{
	Shutdown();
}

bool TabManager::Initialize(RenderUI * pRenderUI)
{
	AdditionTab additionTab(pRenderUI);
	PerpendicularsTab perpTab(pRenderUI);
	DotProductTab dotProductTab(pRenderUI);
	LerpTab lerpTab(pRenderUI);
	return true;
}

bool TabManager::Shutdown()
{
	return true;
}
