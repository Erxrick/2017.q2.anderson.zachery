#pragma once

class RenderUI;
class TabManager
{
public:
	TabManager(RenderUI* pRenderUI);
	~TabManager();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};

