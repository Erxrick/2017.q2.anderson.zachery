#include "GalAI.h"



GalAI::GalAI(int numShip, int rowNum) : shipNumber(numShip), row(rowNum)
{
	GalInit();
}


GalAI::~GalAI()
{
}

void GalAI::GalInit()
{
	shape.ShapeInit(Factory::EnemyGalShip(), Factory::EnemyGalShipSize(), 1);
	m_pos = m_pos + m_startingPos;
	shape.color1 = 255;
	shape.color2 = 0;
	shape.color3 = 0;
}

void GalAI::FollowingWayPoints(float dt)
{
	//std::cout << headedTowards << std::endl;
	Vec2 test = (wayPoints[headedTowards].m_pos - m_pos);
	//std::cout << test.LengthSquared() << std::endl;
	//std::cout << headedTowards << std::endl;
	if (changeTargets == false)
	{
		m_velocity = (wayPoints[headedTowards].m_pos - m_pos) / dt*dt;
		changeTargets = !changeTargets;
	}
	if (test.LengthSquared() < 25)
	{
		changeTargets = !changeTargets;
		if (headedTowards == 1) changeBy = -1;
		else if (headedTowards == 0) changeBy = 1;
		headedTowards += changeBy;
	}
	else if (test.LengthSquared() > 100000)
	{
		changeTargets = !changeTargets;
		if (headedTowards == 1) changeBy = -1;
		else if (headedTowards == 0) changeBy = 1;
		headedTowards += changeBy;
	}

}
bool GalAI::AmIGonnaDie(float dt)
{

	return BM->WithinRange(m_pos, true);
}
bool GalAI::EnemyUpdate(float dt)
{
	if (pathing == false) 
	{
		FollowingWayPoints(dt);
		m_pos = m_pos + (m_velocity * dt);
		transform = Mat3::Translation(m_pos) * (Mat3::Rotation(3.14159f) * Mat3::Scale((float)m_scale));
		//not shooting yet
	} else
	{
		m_pos = Vec2((pathStartX + (30 * sin(m_pos.y / 30))), (m_pos.y + 50 * dt));
		transform = Mat3::Translation(m_pos) * (Mat3::Rotation(3.14159f) * Mat3::Scale((float)m_scale));
		if(alive) Shoot(dt);
		if (m_pos.y > 820.0f)
		{
			m_pos = m_startingPos;
			pathing = false;
		}
	}
	if (alive == true && AmIGonnaDie(dt) == true) 
	{
		alive = false;
	}
	return false;
}

bool GalAI::EnemyDraw(Core::Graphics & g)
{
	if(alive)
	shape.DrawShape(g, transform);
	//for (int i = 0; i < 9; i++)
	//{
	//	wayPoints[i].WaypointDraw(g);
	//}
	return false;
}

Vec2 GalAI::EnemyPosition()
{
	return Vec2(shape.m_shape[Factory::EnemyGalShipSize() - 1].x, shape.m_shape[Factory::EnemyGalShipSize() - 1].y);
}

void GalAI::ChangeMovement()
{
	pathing = true;
	pathStartX = m_pos.x;
}

void GalAI::Shoot(float dt)
{
	static float acumTime = 0;
	acumTime += dt;
	if (acumTime > 0.8f)
	{
		BM->ShootBullet(m_pos, Vec2(0, 100), true);
		acumTime = 0.0f;
	}
}
