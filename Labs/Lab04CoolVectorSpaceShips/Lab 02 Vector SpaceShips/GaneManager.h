#include "Core.h"
#include "Input.h"
#include "graphics.h"
#include "SpaceShip.h"
#include "Input.h"
#include "EnemyShip.h"
#include "Wall.h"
#include "BulletManager.h"
#include "Bullet.h"
#include "GalPlayer.h"
#include "GalAI.h"
#pragma once

class GaneManager
{
public:
	GaneManager(int width = 800, int height = 600);
	~GaneManager();

	bool IsInitialized() const { return m_isInitialized; };
	bool RunGame();

	void Drawer(Core::Graphics& g);
	bool Updater(float dt);
	
	static void Draw(Core::Graphics& g);
	static bool Update(float dt);
	Vec2* getMousePosition() { return &mousePosition; };

	BulletManager BM;
	SpaceShip playerShip;
	GalPlayer galShip;
	int numGalShips{ 36 };
	GalAI enemyGal[36] = { GalAI(1, 1), GalAI(2, 1), GalAI(3, 1), GalAI(4, 1), GalAI(5, 1), GalAI(6, 1), GalAI(7, 1), GalAI(8, 1), GalAI(9, 1),
		GalAI(1, 2), GalAI(2, 2), GalAI(3, 2), GalAI(4, 2), GalAI(5, 2), GalAI(6, 2), GalAI(7, 2), GalAI(8, 2), GalAI(9, 2) ,
		GalAI(1, 3), GalAI(2, 3), GalAI(3, 3), GalAI(4, 3), GalAI(5, 3), GalAI(6, 3), GalAI(7, 3), GalAI(8, 3), GalAI(9, 3),
		GalAI(1, 4), GalAI(2, 4), GalAI(3, 4), GalAI(4, 4), GalAI(5, 4), GalAI(6, 4), GalAI(7, 4), GalAI(8, 4), GalAI(9, 4) };
	EnemyShip enemyShip[7];
	int numEnemyShips{ 7 };
	Wall *wall1;
	Wall *wall2;
	Wall *wall3;
	int wave{ 1 };

private: 
	bool Initialize();
	bool Shutdown();

	void Wrapper();

	void FPS(float dt);
	bool governer(float dt);

	Factory fac;
	Core::Input m_input;
	Vec2 mousePosition{ 0.0f, -10.0f };

	bool m_isInitialized{ false };
	int m_screenWidth;
	int m_screenHeight;
	int m_fps;
	bool m_switch{ false };
	bool paused{ false };
	bool pauseKeyPressed = false;
	bool galaga{ false };
	//testing
	Vec2 defaultVelocity{ 45.0f,45.0f };
	

private:
};

