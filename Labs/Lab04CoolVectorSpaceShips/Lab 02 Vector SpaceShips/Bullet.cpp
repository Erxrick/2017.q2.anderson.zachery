#include "Bullet.h"



Bullet::Bullet()
{
	pos = Engine::Vec2();
	velocity = Engine::Vec2();
	endPoint = Engine::Vec2();
	timeAlive = 0.0f;
	alive = false;
}


Bullet::~Bullet()
{
}

void Bullet::Update(float dt)
{
	timeAlive += dt;
	if (timeAlive > maxLife) this->Reset();
	pos = pos + velocity * 1.5f * dt;
	endPoint = pos + velocity;
}
void Bullet::Reset()
{
	timeAlive = 0;
	pos = Engine::Vec2();
	velocity = Engine::Vec2();
	alive = false;
}

void Bullet::Draw(Core::Graphics g)
{
	g.SetColor((RGB(255, 255, 255)));
	g.DrawLine(pos.x, pos.y, endPoint.Normalize().x + pos.x , endPoint.Normalize().y + pos.y);
}
