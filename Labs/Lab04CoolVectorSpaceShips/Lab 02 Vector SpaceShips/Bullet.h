#pragma once
#include "GameObject.h"
#include "Vec2.h"
#include "graphics.h"
class Bullet
	:public GameObject
{
public:
	Bullet();
	~Bullet();

	void Update(float dt);
	void Reset();
	void Draw(Core::Graphics g);

	Engine::Vec2 velocity;
	Engine::Vec2 pos;
	Engine::Vec2 endPoint;
	bool friendly{ false };
	float GetX() const { return endPoint.x; };
	float GetY() const { return endPoint.y; };

	float timeAlive{ 0 };
	int maxLife{ 5 };
	bool alive{ false };
};

