#include "Turret.h"
#include "Mat3.h"
#include "Factory.h"


Turret::Turret()
{
	shape.ShapeInit(Factory::Turret(), 11, 1);
}


Turret::~Turret()
{
}

bool Turret::DrawShape(Core::Graphics & g, Engine::Vec2 pos, Engine::Vec2 center)
{
	//g.DrawLine(pos.x + center.x, pos.y + center.y, endPoint.x + pos.x, endPoint.y + pos.y);
	g.SetColor(RGB(0, 255, 0));
	Vec2 vectorToTarget = endPoint;
	Vec2 turretDirection = vectorToTarget.Normalize();
	Vec2 turretPerp = turretDirection.PerpCCW();


	Mat3 rotation = Mat3(turretPerp, turretDirection);
	Mat3 translate = Mat3::Translation(pos + center); // not quite in the right place
	Mat3 scale = Mat3::Scale((float)m_scale);
	m_transform = translate * (rotation * scale);	//SRT is the proper order here
	shape.DrawShape(g, m_transform);

	return true;
}
void Turret::SetColor(int c1, int c2, int c3)
{
	shape.color1 = c1;
	shape.color2 = c2;
	shape.color3 = c3;
}

