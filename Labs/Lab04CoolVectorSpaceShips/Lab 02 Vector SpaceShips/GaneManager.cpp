#include "GaneManager.h"
#include "Input.h"
#include <string>
#include "Factory.h"


GaneManager *manager = nullptr;

GaneManager::GaneManager(int width, int height) 
	: m_screenWidth(width), m_screenHeight(height)
{
	m_isInitialized = Initialize();
	manager = this;
	wall1 = new Wall(Vec2((float)(rand() % m_screenWidth), (float)(rand() % m_screenHeight)), Vec2((float)(rand() % m_screenWidth), (float)(rand() % m_screenHeight)));
	wall2 = new Wall(Vec2((float)(rand() % m_screenWidth), (float)(rand() % m_screenHeight)), Vec2((float)(rand() % m_screenWidth), (float)(rand() % m_screenHeight)));
	wall3 = new Wall(Vec2((float)(rand() % m_screenWidth), (float)(rand() % m_screenHeight)), Vec2((float)(rand() % m_screenWidth), (float)(rand() % m_screenHeight)));

}


GaneManager::~GaneManager()
{
	delete wall1;
	delete wall2;
	delete wall3;
	Shutdown();
}

bool GaneManager::RunGame()
{
	Core::GameLoop();
	return true;
}


void GaneManager::Wrapper()
{
	if (playerShip.ShipPosition().x > (float)m_screenWidth)
	{
		playerShip.ShipUpdate(1.0f, Vec2((-1 * (float)m_screenWidth), 0));
	}
	else if (playerShip.ShipPosition().x < 0)
	{
		playerShip.ShipUpdate(1.0f, Vec2((float)(m_screenWidth), 0));
	}
	else if (playerShip.ShipPosition().y > m_screenHeight)
	{
		playerShip.ShipUpdate(1.0f, Vec2(0, (-1 * (float)m_screenHeight)));
	}
	else if (playerShip.ShipPosition().y < 0)
	{
		playerShip.ShipUpdate(1.0f, Vec2(0, (float)m_screenHeight));
	}
	if (galShip.ShipPosition().x >(float)m_screenWidth)
	{
		galShip.ShipUpdate(1.0f, Vec2((-1 * (float)m_screenWidth), 0));
	}
	else if (galShip.ShipPosition().x < 0)
	{
		galShip.ShipUpdate(1.0f, Vec2((float)(m_screenWidth), 0));
	}
}
void GaneManager::FPS(float dt)
{
	static float timer;
	if (timer < 0.5f)
	{
		timer = timer + dt;
	}
	else {
		timer = 0;
		m_fps = (int)(1 / dt);
	}
}
bool GaneManager::governer(float dt)
{
	static float accumTime = 0.0f;
	accumTime += dt;
	float governer = 0.0001f; // governs frame time
	if (accumTime > governer)
	{
		dt = accumTime;
		accumTime = 0.0f;
	}
	else return false;
	return true;

}

void GaneManager::Drawer(Core::Graphics & g)
{
	g.SetColor(RGB(255, 255, 255));
	if (galaga) {
		g.DrawString(m_screenWidth / 2 + 25, 20, std::to_string(wave).c_str());
		g.DrawString(m_screenWidth/2 - 25, 20, "Wave:");
	}
	g.DrawString(m_screenWidth - 50, 50, std::to_string(m_fps).c_str());
	if (!galaga)
	{
		g.DrawString(m_screenWidth / 2 - 90, 15, "Press Escape to Play StarShip Troopers");
		playerShip.ShipDraw(g);
		for (int i = 0; i < numEnemyShips; i++)
		{
			enemyShip[i].EnemyDraw(g);
		}
		wall1->Draw(g);
		wall2->Draw(g);
		wall3->Draw(g);
	}
	else
	{
		galShip.ShipDraw(g);
		for (int i = 0; i < numGalShips; i++)
		{
			enemyGal[i].EnemyDraw(g);
		}
	}
	BM.Draw(g);

}
bool GaneManager::Updater(float dt)
{
	//if (!governer(dt)) return false;
	
	bool previous = pauseKeyPressed;
	if (GetAsyncKeyState(VK_SPACE) != 0)
	{
		pauseKeyPressed = true;
	}
	else 
	{
		pauseKeyPressed = false;
	}
	if ((GetAsyncKeyState(VK_SPACE) == 0) && (pauseKeyPressed == false) && previous == true) paused = !paused;
	
	FPS(dt);
	
	if (Core::Input::IsPressed(Core::Input::KEY_ESCAPE))
	{
		galaga = true;	//use this right here
		BM.Reset();
	}
	if (paused == false)
	{
		if (!galaga) {
				mousePosition = Vec2((float)m_input.GetMouseX(), (float)m_input.GetMouseY());
				playerShip.TurretLockOn(getMousePosition());
				playerShip.ShipUpdate(dt);
				for (int i = 0; i < numEnemyShips; i++)
				{
					enemyShip[i].EnemyUpdate(dt);
				}
				wall1->Update(dt, playerShip);
				wall2->Update(dt, playerShip);
				wall3->Update(dt, playerShip);
		}
		else
		{
			static float enemyTimeForShooting = 0;
			enemyTimeForShooting += dt;

			static int numdead = 0;
			for (int i = 0; i < numGalShips; i++)
			{
				if (enemyGal[i].alive == false) numdead++;
			}
			static float speedPerRound = 0.5;
			if (numdead == numGalShips) {
				for (int i = 0; i < numGalShips; i++)
				{
					enemyGal[i].m_pos = enemyGal[i].m_startingPos;
					enemyGal[i].alive = true;
				}
				if(speedPerRound < 4)
				speedPerRound *= 1.5;
				wave++;
			}
			numdead = 0;
			galShip.ShipUpdate(dt);
			
			if (enemyTimeForShooting > (7/speedPerRound))
			{
				int randomShip = rand() % numGalShips;
				bool next = true;
				for (int i = 0; i < numGalShips; i++)
				{
					if (randomShip > numGalShips) randomShip = randomShip % numGalShips;
					if (enemyGal[randomShip].alive == false) randomShip++;
					else next = false;
				}
				if (enemyGal[randomShip].pathing == false)
				enemyGal[randomShip].ChangeMovement();
				enemyTimeForShooting = 0;
			}
			for (int i = 0; i < numGalShips; i++)
			{
				enemyGal[i].EnemyUpdate(dt);
			}
		}
		BM.Update(dt);
	}
	Wrapper();
	return false;
}


bool GaneManager::Shutdown()
{
	Core::Shutdown();
	return true;
}

bool GaneManager::Update(float dt)
{

	manager->Updater(dt);
	return false;
}
void GaneManager::Draw(Core::Graphics& g)
{

	manager->Drawer(g);
}

bool GaneManager::Initialize()
{
	Core::Init("Starship Troopers", m_screenWidth, m_screenHeight, 2500);
	Core::RegisterUpdateFn(Update);
	Core::RegisterDrawFn(Draw);
	for (int i = 0; i < numEnemyShips; i++)
	{
		enemyShip[i].TurretLockOn(playerShip.ShipPositionPointer());
		enemyShip[i].BM = &BM;
		if (i > 0) {
			enemyShip[i].parentShip = &enemyShip[i - 1];
			enemyShip[i].m_scale = enemyShip[i - 1].m_scale - 1;
			enemyShip[i].m_pos = enemyShip[i - 1].m_pos - Vec2(30.0f, 30.0f);
			enemyShip[i].speedOfRot = enemyShip[i - 1].speedOfRot * 1.3f;
		}
	}
	playerShip.TurretLockOn(getMousePosition());
	playerShip.BM = &BM;
	galShip.BM = &BM;
	for (int i = 0; i < numGalShips; i++)
	{
		enemyGal[i].BM = &BM;
	}
	return true;
}