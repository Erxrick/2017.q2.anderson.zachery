#include "Vec2.h"
#include "Mat3.h"
#include "Core.h"
#include <iostream>
#pragma once


class Shape
{
public:
	Shape();
	~Shape();


	int GetPointsofShape() { return m_numShapePoints; };
	void DrawShape(Core::Graphics& g, Engine::Mat3 transform);
	void DrawShape(Core::Graphics & g, const Engine::Vec2 & position, int scale);
	bool ShapeInit(Engine::Vec2* shape, int numPoints, int scaler);
	Engine::Vec2* m_shape = nullptr;
	int color1{ 255 };
	int color2{ 255 };
	int color3{ 255 };
	
private:
	Engine::Vec2 shape[200];

	int m_numShapePoints;
	

};

