#pragma once
#include "WayPoint.h"
#include "Vec2.h"
#include "Wall.h"

using namespace Engine;
class Factory
{
public:
	static int SpaceShipSize();
	static Vec2* SpaceShipShape();

	static int EnemySpaceShipSize();
	static Vec2* EnemySpaceShipShape();

	static int EnemyGalShipSize();
	static Vec2* EnemyGalShip();

	static Vec2 * Turret();
	
	static Vec2* WaypointShape();
	static int WaypointSize();

	//Wall* GenerateWall(int screenWidth, int screenHeight);
};

