#include "WayPoint.h"



WayPoint::WayPoint(int num, bool isRow, int row)
	:isRow(isRow), row(row)
{
	Init(num);
}


WayPoint::~WayPoint()
{
}

bool WayPoint::WaypointDraw(Core::Graphics g)
{
	shape.DrawShape(g, m_pos, 1);
	return true;
}


void WayPoint::Init(int num)
{
	order = num;
	shape.ShapeInit(Factory::WaypointShape(), Factory::WaypointSize(), 1);
	if (!isRow)
	{
		m_pos = m_pos + Vec2((float)(rand() % 800), (float)(rand() % 600));
	}
	else
	{
		m_pos = m_pos + Vec2(order * 75.0f, row * 50.0f);
	}
}
