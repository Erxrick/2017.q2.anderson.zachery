#pragma once
#include "Core.h"
#include "Vec2.h"
#include "GameObject.h"
#include "SpaceShip.h"
class SpaceShip;
class Wall :
	public GameObject
{
public:
	Wall(Engine::Vec2 origin, Engine::Vec2 endPoint);
	~Wall();


	Engine::Vec2 m_origin;
	Engine::Vec2 m_endPoint;
	//perpendicular
	Engine::Vec2 normalVecWay1;
	Engine::Vec2 normalVecWay2;

	void Update(float dt, SpaceShip& ship);

	bool Draw(Core::Graphics& g);
};

