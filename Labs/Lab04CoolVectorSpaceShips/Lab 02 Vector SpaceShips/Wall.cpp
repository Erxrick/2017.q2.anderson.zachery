#include "Wall.h"
#include "Vec2.h"


Wall::Wall(Engine::Vec2 origin, Engine::Vec2 endPoint) 
	: m_origin(origin), m_endPoint(endPoint)
{
	normalVecWay1 = (m_origin - m_endPoint).PerpCW();
	normalVecWay2 = (m_origin - m_endPoint).PerpCCW();

}


Wall::~Wall()
{
}





void Wall::Update(float dt, SpaceShip& ship)
{

	for (int  i = 0; i < 100; i++)
	{
		Engine::Vec2 distanceToShip1 = Engine::Lerp(m_origin - ship.ShipPosition(), m_endPoint - ship.ShipPosition(), i / 100.0f);
		if (distanceToShip1.LengthSquared() < 100)
		{
			//rejection = v1 - ((v1 * v2.Normalize()) * v2.Normalize());
		//	std::cout << ((distanceToShip1 *(m_origin - m_endPoint).Normalize()) * (m_origin - m_endPoint).Normalize().Normalize()).Normalize() << normalVecWay1.Normalize() << std::endl;

			Vec2 tempVec = distanceToShip1 - ((distanceToShip1 *(m_origin - m_endPoint).Normalize()) * (m_origin - m_endPoint).Normalize());
			
			//std::cout << otherVec << normalVecWay1.Normalize() << std::endl;
			if ((tempVec.Normalize() == normalVecWay1.Normalize())) {
				Vec2 newVelocity = distanceToShip1 - distanceToShip1 * (distanceToShip1 * normalVecWay1) / distanceToShip1.LengthSquared();
				
				ship.ShipUpdate(dt, newVelocity);
			}
			else {
				Vec2 newVelocity = distanceToShip1 - distanceToShip1 * (distanceToShip1 * normalVecWay2) / distanceToShip1.LengthSquared();
				//std::cout << distanceToShip1.LengthSquared() << std::endl;
				ship.ShipUpdate(dt, newVelocity);
			}
		}
	}

	
}



bool Wall::Draw(Core::Graphics & g)
{
	g.SetColor(RGB(0, 255, 0));
	g.DrawLine(m_origin.x, m_origin.y, m_endPoint.x, m_endPoint.y);
	return true;
}
