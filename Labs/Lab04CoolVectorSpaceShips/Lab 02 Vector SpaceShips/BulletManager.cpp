#include "BulletManager.h"
#include <string>



BulletManager::BulletManager()
{
	for (int i = 0; i < 400; i++)
	{
		bullets[i] = new Bullet();
	}
}


BulletManager::~BulletManager()
{
	for (int i = 0; i < 400; i++)
	{
		delete bullets[i];
	}
}

void BulletManager::ShootBullet(Engine::Vec2 pos, Engine::Vec2 velocity)
{
	for (int i = 0; i < 400; i++)
	{
		if (bullets[i]->alive == false)
		{
			bullets[i]->alive = true;
			bullets[i]->pos = pos;
			bullets[i]->velocity = velocity;
			return;
		}
	}
}
void BulletManager::ShootBullet(Engine::Vec2 pos, Engine::Vec2 velocity, bool friendly)
{
	for (int i = 0; i < 400; i++)
	{
		if (bullets[i]->alive == false)
		{
			bullets[i]->alive = true;
			bullets[i]->pos = pos;
			bullets[i]->velocity = velocity;
			bullets[i]->friendly = friendly;
			return;
		}
	}
}

bool BulletManager::WithinRange(Engine::Vec2 pos, bool AIOrPlayer)
{
	for (int i = 0; i < 400; i++)
	{
		if (bullets[i]->alive == true && bullets[i]->friendly != AIOrPlayer) {
			Engine::Vec2 temp = bullets[i]->pos - pos;
			if (temp.LengthSquared() < 361 && temp.LengthSquared() > -361) {
				bullets[i]->Reset();
				score += 75;
				return true;
			}
		}
	}
	return false;
}

void BulletManager::Reset()
{
	for (int i = 0; i < 400; i++)
	{
		bullets[i]->alive = false;
	}
	galaga = true;
}

void BulletManager::Update(float dt)
{
	for (int i = 0; i < 400; i++)
	{
		if (bullets[i]->alive == true)
		bullets[i]->Update(dt);
	}
}
void BulletManager::Draw(Core::Graphics g)
{
	if (galaga == true) {
		g.SetColor(RGB(255, 255, 255));
		g.DrawString(250, 20, std::to_string(score).c_str());
		g.DrawString(200, 20, "Score:");
	}
	for (int i = 0; i < 400; i++)
	{
		if (bullets[i]->alive == true) bullets[i]->Draw(g);
	}
}
