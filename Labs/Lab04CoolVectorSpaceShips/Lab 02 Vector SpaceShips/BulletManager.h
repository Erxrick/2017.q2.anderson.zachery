#pragma once
#include "Bullet.h"
#include "graphics.h"
class BulletManager
{
public:
	BulletManager();
	~BulletManager();


	Bullet *bullets[400];
	void ShootBullet(Engine::Vec2 pos, Engine::Vec2 velocity);
	void ShootBullet(Engine::Vec2 pos, Engine::Vec2 velocity, bool friendly);

	bool WithinRange(Engine::Vec2 pos, bool AIOrPlayer);
	void Reset();
	void Update(float dt);
	void Draw(Core::Graphics g);
	int score{ 0 };
	bool galaga{ false };
};

