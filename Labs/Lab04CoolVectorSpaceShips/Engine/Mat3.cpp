#include "Mat3.h"
namespace Engine {
	 
	Mat3 Engine::Mat3::Rotation(float degrees)
	{
		degrees *= -1;
		return Mat3(cos(degrees), -sin(degrees), 0,
					sin(degrees), cos(degrees), 0,
					0,				0,			1);
	}

	Mat3 Engine::Mat3::Scale(float scale)
	{
		return Mat3(scale,	0,		0,
					0,		scale,	0,
					0,		0,		1);
	}

	Mat3 Engine::Mat3::Scale(float scaleX, float scaleY)
	{
		return Mat3(scaleX,		0, 0,
					0,		scaleY, 0,
					0,			0, 1);
	}

	Mat3 Engine::Mat3::Scale(Vec2 scale)
	{
		return Mat3(scale.x, 0, 0,
						0,	scale.y, 0,
						0,	0, 1);
	}

	Mat3 Engine::Mat3::ScaleX(float scale)
	{
		return Mat3(scale, 0, 0,
			0, 1, 0,
			0, 0, 1);
	}

	Mat3 Engine::Mat3::ScaleY(float scale)
	{
		return Mat3(1, 0, 0,
					0, scale, 0,
					0, 0, 1);
	}

	Mat3 Engine::Mat3::Translation(float x, float y)
	{
		return Mat3(1, 0, x,
					0, 1, y,
					0, 0, 1);
	}

	Mat3 Engine::Mat3::Translation(Vec2& vec)
	{
		return Mat3(1, 0, vec.x,
					0, 1, vec.y,
					0, 0, 1);
	}

	Mat3 Engine::Mat3::operator*(Mat3& matrix3) const
	{
		return Mat3(
		((this->matrixArray[0]*matrix3.matrixArray[0]) +
	/*0*/	(this->matrixArray[1]*matrix3.matrixArray[3]) +
			(this->matrixArray[2] * matrix3.matrixArray[6])),
		((this->matrixArray[0] * matrix3.matrixArray[1]) +
	/*1*/	(this->matrixArray[1] * matrix3.matrixArray[4]) +
			(this->matrixArray[2] * matrix3.matrixArray[7])),
		((this->matrixArray[0] * matrix3.matrixArray[2]) +
	/*2*/	(this->matrixArray[1] * matrix3.matrixArray[5]) +
			(this->matrixArray[2] * matrix3.matrixArray[8])),

		((this->matrixArray[3] * matrix3.matrixArray[0]) +
	/*3*/	(this->matrixArray[4] * matrix3.matrixArray[3]) +				
			(this->matrixArray[5] * matrix3.matrixArray[6])),
		((this->matrixArray[3] * matrix3.matrixArray[1]) +
	/*4*/	(this->matrixArray[4] * matrix3.matrixArray[4]) +
			(this->matrixArray[5] * matrix3.matrixArray[7])),
		((this->matrixArray[3] * matrix3.matrixArray[2]) +
	/*5*/	(this->matrixArray[4] * matrix3.matrixArray[5]) +
			(this->matrixArray[5] * matrix3.matrixArray[8])),

		((this->matrixArray[6] * matrix3.matrixArray[0]) +
	/*6*/	(this->matrixArray[7] * matrix3.matrixArray[3]) +
			(this->matrixArray[8] * matrix3.matrixArray[6])),
		((this->matrixArray[6] * matrix3.matrixArray[1]) +
	/*7*/	(this->matrixArray[7] * matrix3.matrixArray[4]) +
			(this->matrixArray[8] * matrix3.matrixArray[7])),
		((this->matrixArray[6] * matrix3.matrixArray[2]) +
	/*8*/	(this->matrixArray[7] * matrix3.matrixArray[5]) +
			(this->matrixArray[8] * matrix3.matrixArray[8]))		
		);
	}
	//uses 1 for w value
	Vec2 Engine::Mat3::operator*(Vec2& vec) const
	{
		return Vec2((this->matrixArray[0]*vec.x + this->matrixArray[1] * vec.y + this->matrixArray[2]), (this->matrixArray[3] * vec.x + this->matrixArray[4] * vec.y + this->matrixArray[5]));
	}
	Vec3 Engine::Mat3::operator*(Vec3& vec) const
	{
		return Vec3((this->matrixArray[0] * vec.x + this->matrixArray[1] * vec.y + this->matrixArray[2] * vec.z), (this->matrixArray[3] * vec.x + this->matrixArray[4] * vec.y + this->matrixArray[5] * vec.z), (this->matrixArray[6] * vec.x + this->matrixArray[7] * vec.y + this->matrixArray[8] * vec.z));
	}
	//uses 0 for w value
	Vec2 Engine::Mat3::Mult(Vec2& vec) const
	{
		return Vec2((this->matrixArray[0] * vec.x + this->matrixArray[1] * vec.y), (this->matrixArray[3] * vec.x + this->matrixArray[4] * vec.y));
	}

	std::ostream & Mat3::Output(std::ostream & os) const
	{
		os << "Matrix: " << std::endl;
		os << "[" << matrixArray[0] << ", " << matrixArray[1] << ", " << matrixArray[2] << ", " << std::endl;
		os << "[" << matrixArray[3] << ", " << matrixArray[4] << ", " << matrixArray[5] << ", " << std::endl;
		os << "[" << matrixArray[6] << ", " << matrixArray[7] << ", " << matrixArray[8] << ", " << std::endl;
		return os;
	}
	bool Mat3::operator==(const Mat3 & right) const
	{
		for (int i = 0; i < 9; i++)
		{
			if ((!CompareFloats(this->matrixArray[i], right.matrixArray[i]))) return false;

		}
		return true;
	}

	bool Mat3::CompareFloats(float a, float b)
	{
		float range = 0.00001f;
		float diff = 0.0f;
		if (a > b)
		{
			diff = a - b;
		}
		else
		{
			diff = b - a;
		}
		return (range > diff);
	}
}
