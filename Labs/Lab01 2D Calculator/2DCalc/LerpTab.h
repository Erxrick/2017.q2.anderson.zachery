#pragma once

class RenderUI;
class LerpTab
{
public:
	LerpTab(RenderUI* pRenderUI);
	~LerpTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};
