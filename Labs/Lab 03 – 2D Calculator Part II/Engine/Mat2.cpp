#include "Mat2.h"

namespace Engine {


	Mat2::~Mat2()
	{
	}
	Mat2 Mat2::Rotate(float degrees)
	{
		return Mat2(Vec2(cos(-degrees), sin(-degrees)), Vec2(-sin(-degrees), cos(-degrees)));
	}
	Mat2 Mat2::Scale(float scale)
	{
		return Mat2(Vec2(scale, 0.0f), Vec2(0.0f, scale));
	}
	Mat2 Mat2::Scale(Vec2& scale)
	{
		return Mat2(Vec2(scale.x, 0.0f), Vec2(0.0f, scale.y));
	}
	Mat2 Mat2::Scale(float scaleX, float scaleY)
	{
		return Mat2(Vec2(scaleX, 0.0f), Vec2(0.0f, scaleY));
	}
	Mat2 Mat2::ScaleX(flaot scale)
	{
		return Mat2(Vec2(scale, 0.0f), Vec2(0.0f, 1.0f));
	}
	Mat2 Mat2::ScaleY(flaot scale)
	{
		return Mat2(Vec2(1.0f, 0.0f), Vec2(0.0f, scale));
	}
	Mat2 Mat2::operator*(Mat2 & mat2) const
	{
		return Mat2(Vec2(((this->matrixArray[0] * mat2.matrixArray[0]) + (this->matrixArray[1] * mat2.matrixArray[2])),
						((this->matrixArray[2] * mat2.matrixArray[0]) + (this->matrixArray[3] * mat2.matrixArray[2]))), 
					Vec2(((this->matrixArray[0] * mat2.matrixArray[1]) + (this->matrixArray[1] * mat2.matrixArray[3])), 
						((this->matrixArray[2] * mat2.matrixArray[1]) + (this->matrixArray[3] * mat2.matrixArray[3]))));


	}
	Vec2 Mat2::operator*(Vec2 & vec2) const
	{
		return Vec2(((this->matrixArray[0] * vec2.x) + (this->matrixArray[1] * vec2.y)),
			((this->matrixArray[2] * vec2.x) + (this->matrixArray[3] * vec2.y)));
	}
	Mat2 Mat2::operator*(flaot scale) const
	{
		return Mat2(Vec2((this->matrixArray[0] * scale), (this->matrixArray[2] * scale)), Vec2((this->matrixArray[1] * scale), (this->matrixArray[3] * scale)));
	}
	bool Mat2::operator==(const Mat2 & right) const
	{
		for (int i = 0; i < 4; i++)
		{
			if ((!CompareFloats(this->matrixArray[i], right.matrixArray[i]))) return false;
			
		}
		return true;
	}

	bool Mat2::CompareFloats(float a, float b)
	{
		float range = 0.00001f;
		float diff = 0.0f;
		if (a > b)
		{
			diff = a - b;
		}
		else
		{
			diff = b - a;
		}
		return (range > diff);
	}
}