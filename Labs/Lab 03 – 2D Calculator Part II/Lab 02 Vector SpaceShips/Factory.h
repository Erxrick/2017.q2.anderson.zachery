#pragma once
#include "WayPoint.h"
#include "Vec2.h"
#include "Wall.h"

using namespace Engine;
class Factory
{
public:
	static int SpaceShipSize();
	static Vec2* SpaceShipShape();

	static int EnemySpaceShipSize();
	static Vec2* EnemySpaceShipShape();
	
	static Vec2* WaypointShape();
	static int WaypointSize();

	//Wall* GenerateWall(int screenWidth, int screenHeight);
};

