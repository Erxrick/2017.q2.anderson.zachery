#pragma once
#include "GameObject.h"
#include "Vec2.h"
class Turret :
	public GameObject
{
public:
	Turret();
	~Turret();

	bool DrawShape(Core::Graphics& g, Engine::Vec2 pos, Engine::Vec2 center);

	bool playerOrEnemy;
	Engine::Vec2 origin;
	Engine::Vec2 endPoint;
	Engine::Vec2* lockedOnto;
};

