#include "Vec2.h"
#include "Core.h"
#include <iostream>
#pragma once


class Shape
{
public:
	Shape();
	~Shape();


	int GetPointsofShape() { return m_numShapePoints; };
	void DrawShape(Core::Graphics& g, const Engine::Vec2& position);
	bool ShapeInit(Engine::Vec2* shape, int numPoints, int scaler);
	Engine::Vec2* m_shape = nullptr;

private:
	int m_numShapePoints;
	

};

