#include "Factory.h"
#include "Vec2.h"
#include "WayPoint.h"

	//0.5, 0.5 = center
Engine::Vec2 spaceShip[] =
{
	{ -5,-4 }, { -5,1 },
	{ -5,1 },{ -4,1 },
	{ -4,1 },{ -4,-1 },
	{ -4,-1 },{ -3,-1 },
	{ -3,-1 },{ -3,4 },
	{ -3,4 },{ -2,4 },
	{ -2,4 },{ -2,2 },
	{ -2,2 },{ -1,2 },
	{ -1,2 },{ -1,4 },
	{ -1,4 },{ 0,4 },
	{ 0,4 },{ 0,7 },
	{ 0,7 },{ 1,7 },
	{ 1,7 },{ 1,4 },
	{ 1,4 },{ 2,4 },
	{ 2,4 },{ 2,2 },
	{ 2,2 },{ 3,2 },
	{ 3,2 },{ 3,4 },
	{ 3,4 },{ 4,4 },
	{ 4,4 },{ 4,-1 },
	{ 4,-1 },{ 5,-1 },
	{ 5,-1 },{ 5,1 },
	{ 5,1 },{ 6,1 },
	{ 6,1 },{ 6,-4 },
	{ 6,-4 },{ 5,-4 },
	{ 5,-4 },{ 5,-3 },
	{ 5,-3 },{ 4,-3 },
	{ 4,-3 },{ 4,-2 },
	{ 4,-2 },{ 3,-2 },
	{ 3,-2 },{ 3,-3 },
	{ 3,-3 },{ 1,-3 },
	{ 1,-3 },{ 1,-4 },
	{ 1,-4 },{ 0,-4 },
	{ 0,-4 },{ 0,-3 },
	{ 0,-3 },{ -2,-3 },
	{ -2,-3 },{ -2,-2 },
	{ -2,-2 },{ -3,-2 },
	{ -3,-2 },{ -3,-3 },
	{ -3,-3 },{ -4,-3 },
	{ -4,-3 },{ -4,-4 },
	{ -4,-4 },{ -5,-4 },
	{0.5f, 0.5f}
};

Engine::Vec2 enemyShip[] =
{
	{-2.0f, -2.0f}, {3.0f, 1.0f},
	{3.0f, 1.0f}, {-3.0f, 1.0f},
	{-3.0f, 1.0f}, {2.0f, -2.0f},
	{2.0f, -2.0f}, {0.0f, 3.0f},
	{0.0f, 3.0f}, {-2.0f, -2.0f},
	{0.0f, 0.0f}
};
Engine::Vec2 wayPointShape[] =
{
	{ 1.0f, 1.0f },{ 2.0f, 1.0f },
	{ 2.0f, 1.0f },{ 2.0f, 2.0f },
	{ 2.0f, 2.0f },{ 1.0f, 2.0f },
	{ 1.0f, 2.0f },{ 1.0f, 1.0f },
	{ 1.5, 1.5f }
};
int Factory::SpaceShipSize()
{
	return 81;
}

Vec2 * Factory::SpaceShipShape()
{
	return spaceShip;
}

int Factory::EnemySpaceShipSize()
{
	return 11;
}

Vec2 * Factory::EnemySpaceShipShape()
{
	return enemyShip;
}

Vec2 * Factory::WaypointShape()
{
	return wayPointShape;
}

int Factory::WaypointSize()
{
	return 9;
}
//Wall* Factory::GenerateWall(int screenWidth, int screenHeight)
//{
//	return new Wall(Vec2(rand() % screenWidth, rand() % screenHeight), Vec2(rand() % screenWidth, rand() % screenHeight));
//}

//WayPoint * Factory::NewWaypoint()
//{
//	static int count;
//	WayPoint* wp = new WayPoint(count);
//	count++;
//	return wp;
//}

