#include "TabManager.h"
#include "RenderUI.h"
#include "AdditionTab.h"
#include "PerpendicularsTab.h"
#include "DotProductTab.h"
#include "LerpTab.h"
#include "AffineTranformTab.h"
#include "MatrixMultiplyTab.h"
#include "MatrixTransformTab.h"


TabManager::TabManager(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


TabManager::~TabManager()
{
	Shutdown();
}

bool TabManager::Initialize(RenderUI * pRenderUI)
{
	AdditionTab additionTab(pRenderUI);
	PerpendicularsTab perpTab(pRenderUI);
	DotProductTab dotProductTab(pRenderUI);
	LerpTab lerpTab(pRenderUI);
	AffineTranformTab affineTab(pRenderUI);
	MatrixMultiplyTab matMultTab(pRenderUI);
	MatrixTransformTab MatTransTab(pRenderUI);
	return true;
}

bool TabManager::Shutdown()
{
	return true;
}
