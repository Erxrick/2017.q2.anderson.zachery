#pragma once

class RenderUI;
class DotProductTab
{
public:
	DotProductTab(RenderUI* pRenderUI);
	~DotProductTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};

