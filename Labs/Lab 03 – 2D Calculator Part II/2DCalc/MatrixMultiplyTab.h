#pragma once

class RenderUI;
class MatrixMultiplyTab
{
public:
	MatrixMultiplyTab(RenderUI* pRenderUI);
	~MatrixMultiplyTab();

	bool IsInitialized() const { return m_isInitialized; };


private:
	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();


	bool m_isInitialized = false;
};
