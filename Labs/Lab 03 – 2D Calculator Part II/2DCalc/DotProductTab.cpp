#include "DotProductTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;
namespace
{
	Vec2 v1;
	Vec2 v2;
	Vec2 projection;
	Vec2 rejection;

	void DotProductTabCallback(const DotProductData& data)
	{
		v1 = Vec2(data.v1i, data.v1j);
		v2 = Vec2(data.v2i, data.v2j);

		if (data.projectOntoLeftVector)
		{
			projection = (v1 * v2.Normalize()) * v2.Normalize();
			rejection = v1 - ((v1 * v2.Normalize()) * v2.Normalize());
		}
		else {
			projection = (v2 * v1.Normalize()) * v1.Normalize();
			rejection = v2 - ((v2 * v1.Normalize()) * v1.Normalize());
		}


		
	}
}

DotProductTab::DotProductTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


DotProductTab::~DotProductTab()
{
	Shutdown();
}

bool DotProductTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setDotProductData(v1.Pos(), v2.Pos(), projection.Pos(), rejection.Pos(), DotProductTabCallback);
	return false;
}

bool DotProductTab::Shutdown()
{
	return false;
}