#include "CppUnitTest.h"
#include "..//Engine/Mat3.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<>
			std::wstring ToString<Mat3>(const Mat3 &mat3) {
				return (L"" + std::to_wstring(mat3.matrixArray[0])) + (L", " + std::to_wstring(mat3.matrixArray[1])) + (L", " + std::to_wstring(mat3.matrixArray[2])) +
					(L", " + std::to_wstring(mat3.matrixArray[3])) +  (L", " + std::to_wstring(mat3.matrixArray[4])) + (L", " + std::to_wstring(mat3.matrixArray[5])) + 
						(L", " + std::to_wstring(mat3.matrixArray[6])) + (L", " + std::to_wstring(mat3.matrixArray[7])) + (L", " + std::to_wstring(mat3.matrixArray[8]));
			}
			template<>
			std::wstring ToString<Vec2>(const Vec2 &vec2) {
				return (L"x = " + std::to_wstring(vec2.x) + L" y = " + std::to_wstring(vec2.y));
			}
		}
	}
}

namespace Mat3Test
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(NewMat3Identity)
		{
			Mat3 identity = Mat3();
			Mat3 realIdentity = Mat3(1, 0, 0,
				0, 1, 0,
				0, 0, 1);
			Assert::AreEqual(identity, realIdentity, L"The default constructor did not give an identity matrix");
		}
		TEST_METHOD(RotationTest)
		{
			Mat3 rotated = Mat3::Rotation(45);

			Mat3 manualRotation = Mat3(cos(-45), -sin(-45), 0, sin(-45), cos(-45), 0, 0, 0, 1);

			Assert::AreEqual(rotated, manualRotation, L"The rotation was not completed properly");
		}
		TEST_METHOD(ScaleMat3)
		{
			Mat3 scaleFloat = Mat3::Scale(7);
			Mat3 manScaleFloat = Mat3(7, 0, 0, 0, 7, 0, 0, 0, 1);
			Assert::AreEqual(scaleFloat, manScaleFloat, L"Symm scaling is broken");

			Mat3 scaleFloat2 = Mat3::Scale(7, 3);
			Mat3 manScaleFloat2 = Mat3(7, 0, 0, 0, 3, 0, 0, 0, 1);
			Assert::AreEqual(scaleFloat2, manScaleFloat2, L"Nonuniform 2-var scaling is broken");

			Mat3 scaleFloat3 = Mat3::Scale(Vec2(3,4));
			Mat3 manScaleFloat3 = Mat3(3, 0, 0, 0, 4, 0, 0, 0, 1);
			Assert::AreEqual(scaleFloat3, manScaleFloat3, L"Vector scaling is broken");

			Mat3 scaleFloat4 = Mat3::ScaleX(8) * Mat3::ScaleY(9);
			Mat3 manScaleFloat4 = Mat3(8, 0, 0, 0, 9, 0, 0, 0, 1);
			Assert::AreEqual(scaleFloat4, manScaleFloat4, L"Symm scaling is broken");
		}
		TEST_METHOD(Translation)
		{
			Mat3 trans = Mat3::Translation(10, 10);
			Mat3 ManTrans = Mat3(1, 0, 10, 0, 1, 10, 0, 0, 1);
			Assert::AreEqual(trans, ManTrans, L"Translation matrixes made from floats were not equal");
			
			Mat3 trans2 = Mat3::Translation(Vec2(3, 4));
			Mat3 manTrans2 = Mat3(1, 0, 3, 0, 1, 4, 0, 0, 1);
			Assert::AreEqual(trans2, manTrans2, L"The vec base translation broke");
		}
		TEST_METHOD(MultiplicationTest)
		{
			Mat3 mult1 = Mat3(2, 0, 2, 0, 2, 0, 2, 0, 2);
			Mat3 mult2 = Mat3(3, 0, 3, 0, 3, 0, 3, 0, 3);

			Mat3 result = mult1 * mult2;
			Mat3 actualResult = Mat3(12, 0, 12, 0, 6, 0, 12, 0, 12);
			Assert::AreEqual(result, actualResult, L"The Multiplication between to mat3s broke");
		}
		TEST_METHOD(MultVec2Mat3)
		{
			Mat3 mult1 = Mat3(2, 3, 2, 3, 2, 3, 2, 3, 2);
			Vec2  mult2 = Vec2(7, 8);
			Vec2 result = mult1 * mult2;
			Assert::AreEqual(result, Vec2(40, 40), L"I Suck at vector multiplication");
		}
	};
}